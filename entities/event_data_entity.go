package entities

import "time"

type EventData struct {
	LastInvokedTime time.Time `json:"last_invoked_time"`
	InvokeCount     int       `json:"invoke_count"`
	ActiveContext   *string   `json:"active_context,omitempty"`
	Text            *string   `json:"text,omitempty"`
}
