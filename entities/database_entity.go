package entities

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"log"
)

type DatabaseEntityError string

func (e DatabaseEntityError) Error() string {
	return string(e)
}

const ErrNotFound = DatabaseEntityError("entity not found")

var dirtyEntities = map[string]DatabaseEntity{}
var entitiesCache = map[string]DatabaseEntity{}

type DatabaseEntity interface {
	PrimaryKeyName() string
	SortKeyNames() []string
	TableName() string
	ModelName() string
	EntityName() string
	EntityID() int64
	PrimaryKey() *string
	IsDeleted() bool
}

func cacheKey(m DatabaseEntity) string {
	return fmt.Sprintf("%s-%d", m.TableName(), m.EntityID())
}

func Save(m DatabaseEntity, saveNow bool) {
	if saveNow {
		save(m)
		return
	}
	dirtyEntities[cacheKey(m)] = m
}

func ClearCache() {
	entitiesCache = map[string]DatabaseEntity{}
}

func Flush() {
	entities := dirtyEntities
	dirtyEntities = map[string]DatabaseEntity{}
	for _, e := range entities {
		save(e)
	}
}

func save(m DatabaseEntity) {
	if m.IsDeleted() {
		return
	}
	if m == nil {
		log.Println("tried to update nil entity")
		return
	}
	config.LogDebug("updating %s [%d]", m.EntityName(), m.EntityID())
	b, err := json.Marshal(m)
	if err != nil {
		log.Printf("error updating %s %s [%d]: %v", m.ModelName(), m.EntityName(), m.EntityID(), err)
		return
	}
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#JSON": aws.String("JSON"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":json": {
				S: aws.String(string(b)),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			m.PrimaryKeyName(): {
				N: m.PrimaryKey(),
			},
		},
		TableName:        aws.String(m.TableName()),
		UpdateExpression: aws.String("SET #JSON = :json"),
	}
	_, err = database.Update(input)
	if err != nil {
		log.Printf("update failed: %v\n", err)
	}
	if _, ok := entitiesCache[cacheKey(m)]; ok {
		delete(entitiesCache, cacheKey(m))
		entitiesCache[cacheKey(m)] = m
	}
	return
}

func Get(m DatabaseEntity) (DatabaseEntity, error) {
	if e, ok := entitiesCache[cacheKey(m)]; ok {
		config.LogDebug("%s (%d) returned from cache", m.EntityName(), m.EntityID())
		return e, nil
	}
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			m.PrimaryKeyName(): {
				N: m.PrimaryKey(),
			},
		},
		TableName: aws.String(m.TableName()),
	}
	result, err := database.Get(input)
	if err != nil {
		return nil, err
	}
	if result == nil || result.Item == nil || result.Item["JSON"] == nil {
		config.LogDebug("no item returned for %s %s [%d]", m.ModelName(), m.EntityName(), m.EntityID())
		return nil, ErrNotFound
	}
	js := *result.Item["JSON"].S
	config.LogDebug(js)
	if err = json.Unmarshal([]byte(js), m); err != nil {
		return nil, err
	}
	entitiesCache[cacheKey(m)] = m
	return m, nil
}

func Put(m DatabaseEntity) {
	config.LogDebug("putting %s in db\n", m.EntityName())
	b, err := json.Marshal(m)
	if err != nil {
		log.Printf("error putting %s %s [%d]: %v", m.ModelName(), m.EntityName(), m.EntityID(), err)
		return
	}
	if _, ok := entitiesCache[cacheKey(m)]; ok {
		delete(entitiesCache, cacheKey(m))
		entitiesCache[cacheKey(m)] = m
	}
	input := &dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			m.PrimaryKeyName(): {
				N: m.PrimaryKey(),
			},
			"JSON": {
				S: aws.String(string(b)),
			},
		},
		TableName: aws.String(m.TableName()),
	}
	_, err = database.Put(input)
	if err != nil {
		log.Printf("put failed: %v\n", err)
	}
	return
}

func Delete(m DatabaseEntity) {
	config.LogDebug("deleting %s in db\n", m.EntityName())
	if _, ok := entitiesCache[cacheKey(m)]; ok {
		delete(entitiesCache, cacheKey(m))
	}
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			m.PrimaryKeyName(): {
				N: m.PrimaryKey(),
			},
		},
		TableName:    aws.String(m.TableName()),
		ReturnValues: aws.String("ALL_OLD"),
	}
	_, err := database.Delete(input)
	if err != nil {
		log.Printf("delete failed: %v\n", err)
	}
	return
}
