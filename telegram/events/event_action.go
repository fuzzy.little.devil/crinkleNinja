package events

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
	"runtime/debug"
)

type MatchFunc func(ea *EventAction, message *messaging.Message, bot telegram.Bottable) bool
type ActionFunc func(ea *EventAction, message *messaging.Message, bot telegram.Bottable) error

type EventAction struct {
	Event         *Event
	Name          string
	Help          string
	MatchFuncs    []MatchFunc
	ActionFunc    ActionFunc
	IsExclusive   bool
	IsInStartList bool
	HasData       bool
	EndingContext bool
	Data          *model.EventActionData
}

func (ea *EventAction) IsMatch(message *messaging.Message, bot *telegram.Bot) bool {
	// Disallow other commands during contexts
	if model.CurrentUser.GetActiveContextName() != "" && message.Chat.IsPrivate() {
		// Stop/Cancel receives highest order
		if message.Text == "/stop" || message.Text == "/cancel" {
			if ea.Name != "stop" && ea.Name != "cancel" {
				return false
			}
		}
		if ea.HasCurrentContext(model.CurrentUser) {
			return true
		}
	}

	for _, f := range ea.MatchFuncs {
		if !f(ea, message, bot) {
			return false
		}
	}
	return true
}

func (ea *EventAction) Recover() {
	if r := recover(); r != nil {
		fmt.Println("Event Action -- PANIC RECOVERY", r)
		debug.PrintStack()
	}
	return
}

func (ea *EventAction) Handle(message *messaging.Message, bot *telegram.Bot) error {
	if ea.HasData {
		ea.Data = model.GetOrCreateEventActionData(ea.Name)
	}
	if ea.Name == "CustomCommand" {
		ctx := ea.GetContext(model.CurrentUser)
		if ctx != nil && ctx.Text != nil {
			c := model.GetCommand(*ctx.Text)
			if c != nil {
				ea.Data = &model.EventActionData{}
				ea.Data.Data = c
			}
		}
	}
	defer ea.Recover()
	err := ea.ActionFunc(ea, message, bot)
	if err != nil {
		fmt.Println(err)
		return err
	}
	if bot.Debug {
		log.Printf("%s [%d] executing %s '%s'", model.CurrentUser.UserName, model.CurrentUser.ID, ea.Event.Name, ea.Name)
	}
	return nil
}

func (ea *EventAction) UpdateContext(user *model.User) {
	if user.Events == nil {
		user.Events = map[string]*entities.EventData{}
	}
	d, ok := user.Events[ea.Event.Name]
	if !ok {
		d = &entities.EventData{}
		user.Events[ea.Event.Name] = d
	}
	if config.IsDebug() {
		log.Printf("%s setting context to %s", model.CurrentUser.UserName, ea.Name)
	}
	d.ActiveContext = util.PtrString(ea.Name)
	ea.EndingContext = false
	if ea.Name == "CustomCommand" && ea.Data != nil {
		c, ok := ea.Data.Data.(*model.Command)
		if !ok {
			log.Println("error extracting custom command")
		} else {
			d.Text = util.PtrString(c.Name)
		}
	}
	user.Save()
}

func (ea *EventAction) GetContext(user *model.User) *entities.EventData {
	if user.Events == nil {
		return nil
	}
	return user.Events[ea.Name]
}

func (ea *EventAction) ClearAllContext(user *model.User) {
	if user.Events == nil {
		return
	}
	found := false
	for _, action := range user.Events {
		if action.ActiveContext != nil {
			action.ActiveContext = nil
			found = true
		}
	}
	if found {
		if config.IsDebug() {
			log.Printf("%s clearing context", model.CurrentUser.UserName)
		}
		user.Save()
	}
}

func (ea *EventAction) HasCurrentContext(user *model.User) bool {
	if user.Events == nil {
		user.Events = map[string]*entities.EventData{}
	}
	if d, ok := user.Events[ea.Event.Name]; ok {
		return d.ActiveContext != nil && *d.ActiveContext == ea.Name
	}
	return false
}

func (ea *EventAction) ClearContext(user *model.User) {
	if user.Events == nil {
		user.Events = map[string]*entities.EventData{}
	}
	if d, ok := user.Events[ea.Event.Name]; ok {
		d.ActiveContext = nil
		ea.EndingContext = true
		if config.IsDebug() {
			log.Printf("%s clearing context %s", model.CurrentUser.UserName, ea.Name)
		}
		user.Save()
	}
}
