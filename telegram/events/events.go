package events

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"math"
	"time"
)

type Event struct {
	Name                  string
	UserDelay             time.Duration
	ChatCoolDown          time.Duration
	ChatMaxCount          int
	Actions               map[string]*EventAction
	EnableFloodProtection bool
}

func (e *Event) CanInvoke(user *model.User, chat *model.Chat) bool {
	if user == nil || chat == nil {
		return false
	}
	if !e.EnableFloodProtection {
		return true
	}
	e.EventChatTick(chat)
	if !e.CanUserInvokeEvent(user) || !e.CanChatInvokeEvent(chat) {
		return false
	}
	return true
}

func (e *Event) Invoke(message *messaging.Message, bot *telegram.Bot) (bool, error) {
	if !e.CanInvoke(model.CurrentUser, model.CurrentChat) {
		return false, nil
	}
	var ea *EventAction

	for _, action := range e.Actions {
		if model.CurrentUser.GetActiveContextName() != "" &&
			model.CurrentUser.GetActiveContextName() != action.Name &&
			message.Text != "/stop" && message.Text != "/cancel" {
			continue
		}
		if bot.Debug {
			fmt.Printf("Checking %s\n", action.Name)
		}
		if action.IsMatch(message, bot) {
			ea = action
		}
	}
	if ea == nil {
		return false, nil
	}

	if bot.Debug {
		fmt.Printf("Handling %s\n", ea.Name)
	}
	err := ea.Handle(message, bot)
	if err != nil {
		fmt.Println(err)
		return true, err
	}
	if !model.CurrentChat.IsPrivate() && e.EnableFloodProtection {
		e.UpdateChatEvent(model.CurrentChat)
		e.UpdateUserEvent(model.CurrentUser)
	}
	return true, nil

}

func (e *Event) CanChatInvokeEvent(chat *model.Chat) bool {
	if chat.Events == nil {
		chat.Events = map[string]*entities.EventData{}
	}

	d, ok := chat.Events[e.Name]
	if !ok {
		return true
	}
	return chat.IsPrivate() || d.InvokeCount <= e.ChatMaxCount
}

func (e *Event) EventChatTick(chat *model.Chat) {
	if chat.Events == nil {
		chat.Events = map[string]*entities.EventData{}
	}

	d, ok := chat.Events[e.Name]
	if !ok {
		return
	}
	counts := int(time.Now().Sub(d.LastInvokedTime).Seconds() / e.ChatCoolDown.Seconds())
	if counts > 0 {
		d.InvokeCount = int(math.Max(0, float64(d.InvokeCount-counts)))
		d.LastInvokedTime = time.Now()
		chat.Save()
	}
}

func (e *Event) UpdateChatEvent(chat *model.Chat) {
	if chat.Events == nil {
		chat.Events = map[string]*entities.EventData{}
	}

	d, ok := chat.Events[e.Name]
	if !ok {
		d = &entities.EventData{}
		chat.Events[e.Name] = d
	}
	d.InvokeCount++
	d.LastInvokedTime = time.Now()
	chat.Save()
}

func (e *Event) CanUserInvokeEvent(user *model.User) bool {
	if user.Events == nil {
		user.Events = map[string]*entities.EventData{}
	}

	d, ok := user.Events[e.Name]
	if !ok {
		return true
	}
	return time.Now().After(d.LastInvokedTime.Add(e.UserDelay)) || user.CurrentChatAdmin
}

func (e *Event) UpdateUserEvent(user *model.User) {
	if user.Events == nil {
		user.Events = map[string]*entities.EventData{}
	}

	d, ok := user.Events[e.Name]
	if !ok {
		d = &entities.EventData{}
		user.Events[e.Name] = d
	}
	d.LastInvokedTime = time.Now()
	user.Save()
}

func (e *Event) AddAction(action *EventAction) {
	if e.Actions == nil {
		e.Actions = map[string]*EventAction{}
	}
	action.Event = e
	e.Actions[action.Name] = action
}

var CommandEvent = &Event{
	Name:                  "Command",
	UserDelay:             time.Duration(15 * time.Second),
	ChatCoolDown:          time.Duration(15 * time.Second),
	ChatMaxCount:          4,
	EnableFloodProtection: true,
}

var TriggerEvent = &Event{
	Name:                  "Trigger",
	UserDelay:             time.Duration(30 * time.Second),
	ChatCoolDown:          time.Duration(15 * time.Second),
	ChatMaxCount:          5,
	EnableFloodProtection: true,
}

var ReplyKeyboardEvent = &Event{
	Name: "ReplyKeyboard",
}
