package telegram

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gopkg.in/telegram-bot-api.v4"
	"log"
)

type Bot struct {
	*tgbotapi.BotAPI
	BotAdminIDs []int
}

func (b Bot) AdminIDs() []int {
	return b.BotAdminIDs
}

func (b Bot) ID() int {
	return b.Self.ID
}

func (b Bot) UserName() string {
	return b.Self.UserName
}

type Bottable interface {
	AdminIDs() []int
	ID() int
	UserName() string
	GetFileDirectURL(string) (string, error)
	GetMe() (tgbotapi.User, error)
	IsMessageToMe(tgbotapi.Message) bool
	Send(tgbotapi.Chattable) (tgbotapi.Message, error)
	DeleteMessage(tgbotapi.DeleteMessageConfig) (tgbotapi.APIResponse, error)
	GetWebhookInfo() (tgbotapi.WebhookInfo, error)
}

func NewBot(cfg *config.BotConfig) (*Bot, error) {
	tgBot, err := tgbotapi.NewBotAPI(config.GetSecretString("BotToken"))
	if err != nil {
		return nil, err
	}
	tgBot.Debug = config.IsDebug()
	b := &Bot{
		BotAPI:      tgBot,
		BotAdminIDs: cfg.BotAdminIDs,
	}
	return b, nil
}

// boolean indicates if the handler should continue
// useful for situations where a match should be exclusive
type MessageHandler func(message *messaging.Message, bot *Bot) (bool, error)

var messageHandlers = map[string]MessageHandler{}

func RegisterHandler(name string, handler MessageHandler) {
	messageHandlers[name] = handler
}

func (b *Bot) ProcessMessage(message *messaging.Message) {
	for _, entity := range message.Entities {
		config.LogDebug("[Entities] Type: %s, URL: %s, length: %d, offset: %d, Data: %s", entity.Type, entity.URL, entity.Length, entity.Offset, entity.Data)
	}
	for name, handler := range messageHandlers {
		config.LogDebug("Executing handler: %s\n", name)
		found, err := handler(message, b)
		if err != nil {
			log.Printf("error from handler %s: %v", name, err)
		}
		if found {
			break
		}
	}
}
