package messaging_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/fakes"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
)

var _ = Describe("Message", func() {
	var (
		text     messaging.Message
		sticker  messaging.Message
		photo    messaging.Message
		document messaging.Message
		audio    messaging.Message
		cmd      messaging.Message
	)
	BeforeEach(func() {
		text = messaging.Message{Message: fakes.Message(fakes.FakeMessage)}
		text.Chat = fakes.Chat(fakes.FakePrivateChat)
		text.Text = "test"

		sticker = messaging.Message{Message: fakes.Message(fakes.FakeMessage)}
		sticker.Chat = fakes.Chat(fakes.FakeGroupChat)
		sticker.Sticker = fakes.Sticker(fakes.FakeSticker)

		photo = messaging.Message{Message: fakes.Message(fakes.FakeMessage)}
		photo.Photo = fakes.PhotoSizes()

		document = messaging.Message{Message: fakes.Message(fakes.FakeMessage)}
		document.Document = fakes.Document(fakes.FakeDocument)

		audio = messaging.Message{Message: fakes.Message(fakes.FakeMessage)}
		audio.Audio = fakes.Audio(fakes.FakeAudio)

		cmd = messaging.Message{Message: fakes.Message(fakes.FakeMessage)}
		cmd.IsCommand()
		cmd.Text = fakes.FakeCommandText
		fakes.MessageEntities(&cmd, fakes.FakeCommandEntity)
	})

	Describe("Discovering message type", func() {
		Context("With text", func() {
			It("should be a Text message", func() {
				Expect(text.MessageType().Name).To(Equal(messaging.TextMessage))
			})
		})
		Context("With sticker", func() {
			It("should be a Sticker message", func() {
				Expect(sticker.MessageType().Name).To(Equal(messaging.StickerMessage))
			})
		})
		Context("With photo", func() {
			It("should be a Photo message", func() {
				Expect(photo.MessageType().Name).To(Equal(messaging.PhotoMessage))
			})
		})
		Context("With document", func() {
			It("should be a Document message", func() {
				Expect(document.MessageType().Name).To(Equal(messaging.DocumentMessage))
			})
		})
		Context("With audio", func() {
			It("should be a Audio message", func() {
				Expect(audio.MessageType().Name).To(Equal(messaging.AudioMessage))
			})
		})
		Context("With telegram command", func() {
			It("should be a Bot Command message", func() {
				Expect(cmd.MessageType().Name).To(Equal(messaging.CommandMessage))
			})
		})
	})
})
