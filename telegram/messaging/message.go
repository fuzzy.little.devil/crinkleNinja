package messaging

import (
	"fmt"
	"gopkg.in/telegram-bot-api.v4"
	"strings"
)

func NewMessage(message *tgbotapi.Message) *Message {
	msg := &Message{Message: message}
	var entities []*Entity
	if message.Entities == nil {
		return msg
	}
	for _, ent := range *message.Entities {
		entities = append(entities, NewEntity(msg, &ent))
	}
	msg.Entities = entities
	return msg
}

// NewMessageFromUpdate returns a Message
func NewMessageFromUpdate(update tgbotapi.Update) *Message {
	return NewMessage(update.Message)
}

func NewMessageFromReply(message *Message) *Message {
	if message.ReplyToMessage == nil {
		return nil
	}
	return NewMessage(message.ReplyToMessage)
}

type TrackData struct {
	URL     string
	Artists []string
	Title   string
}

func (td *TrackData) String() string {
	return fmt.Sprintf("%s by %s", td.Title, strings.Join(td.Artists, ", "))
}

// Message decorates a Telegram message
type Message struct {
	*tgbotapi.Message
	messageType *MessageType
	Entities    []*Entity
	Track       *TrackData
}

type Entity struct {
	*tgbotapi.MessageEntity
	Data string
}

func NewEntity(message *Message, entityData *tgbotapi.MessageEntity) *Entity {
	return &Entity{
		MessageEntity: entityData,
		Data:          message.Text[entityData.Offset : entityData.Offset+entityData.Length],
	}
}

// Internal function to generate the name of the chat
func (m *Message) getChatName() string {
	return fmt.Sprintf("(%s) %s", m.Chat.Type, m.Chat.Title)
}

func (m *Message) Reply() *Message {
	return NewMessageFromReply(m)
}

// Internal function to generate a standard prefix for String()
func (m *Message) stringPrefix() string {
	var caption string
	if len(m.Caption) > 0 {
		caption = fmt.Sprintf(" '%s'", m.Caption)
	}
	return fmt.Sprintf("[%s] <%s> [%s]%s", m.getChatName(), m.From.UserName, m.MessageType().Name, caption)
}

// String() implements the Stringer interface
func (m *Message) String() string {
	return fmt.Sprintf("%s %s", m.stringPrefix(), m.MessageType().formatFunc(m))
}

// MessageType lazily assigns and returns the type of message
func (m *Message) MessageType() *MessageType {
	if m.messageType == nil {
		m.messageType = messageTypes[UnknownMessage]
		for _, t := range messageTypes {
			if t.typeFunc(m) {
				m.messageType = t
				break
			}
		}
	}
	return m.messageType
}
