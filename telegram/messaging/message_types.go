package messaging

import (
	"encoding/json"
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/integrations/tracks"
	"log"
	"regexp"
	"strings"
)

// Internal function types for detecting and formatting messages of specific types
type fmtFunc func(*Message) string
type typeFunc func(*Message) bool

// MessageType defines the type of message from an update
type MessageType struct {
	formatFunc fmtFunc
	Name       MessageTypeName
	typeFunc   typeFunc
}

var spotifyMessagePattern = regexp.MustCompile(".*https://open.spotify.com/track/.*")
var tidalMessagePattern = regexp.MustCompile(".*https://tidal.com/browse/track/.*")

// MessageTypeName allows for type-safe handling of message types
type MessageTypeName string

// MessageTypeName definitions
var (
	UnknownMessage  MessageTypeName = "Unknown"
	TextMessage     MessageTypeName = "Text"
	CommandMessage  MessageTypeName = "Command"
	StickerMessage  MessageTypeName = "Sticker"
	DocumentMessage MessageTypeName = "Document"
	AudioMessage    MessageTypeName = "Audio"
	PhotoMessage    MessageTypeName = "Photo"
	TrackMessage    MessageTypeName = "Track"

	messageTypes = map[MessageTypeName]*MessageType{
		UnknownMessage: {
			Name: UnknownMessage,
			formatFunc: func(message *Message) string {
				json, err := json.Marshal(message.Message)
				if err != nil {
					json = []byte(fmt.Sprintf("unable to marshal message to string: %s", err))
				}
				return string(json)
			},
			typeFunc: func(message *Message) bool {
				return false
			},
		},
		CommandMessage: {
			Name: CommandMessage,
			formatFunc: func(message *Message) string {
				return fmt.Sprintf("CMD: %s, ARGS: %s", message.Command(), message.CommandArguments())
			},
			typeFunc: func(message *Message) bool {
				return message.IsCommand()
			},
		},
		TextMessage: {
			Name: TextMessage,
			formatFunc: func(message *Message) string {
				return message.Text
			},
			typeFunc: func(message *Message) bool {
				return len(message.Text) > 0 && !message.IsCommand() && !strings.Contains(message.Text, "http")
			},
		},
		StickerMessage: {
			Name: StickerMessage,
			formatFunc: func(message *Message) string {
				s := message.Sticker
				return fmt.Sprintf("(%s) %s", s.Emoji, s.FileID)
			},
			typeFunc: func(message *Message) bool {
				return message.Sticker != nil
			},
		},
		PhotoMessage: {
			Name: PhotoMessage,
			formatFunc: func(message *Message) string {
				var photos []string
				for _, p := range *message.Photo {
					photos = append(photos, fmt.Sprintf("(%dx%d) %s [%d bytes]", p.Width, p.Height, p.FileID, p.FileSize))
				}
				return strings.Join(photos, ",")
			},
			typeFunc: func(message *Message) bool {
				return message.Photo != nil && len(*message.Photo) > 0
			},
		},
		DocumentMessage: {
			Name: DocumentMessage,
			formatFunc: func(message *Message) string {
				d := message.Document
				return fmt.Sprintf("%s (%s) %s [%d bytes]", d.FileName, d.MimeType, d.FileID, d.FileSize)
			},
			typeFunc: func(message *Message) bool {
				return message.Document != nil
			},
		},
		AudioMessage: {
			Name: AudioMessage,
			formatFunc: func(message *Message) string {
				a := message.Audio
				return fmt.Sprintf("[%s] %s by %s (%d) - %s [%d bytes]", a.MimeType, a.Title, a.Performer, a.Duration, a.FileID, a.FileSize)
			},
			typeFunc: func(message *Message) bool {
				return message.Audio != nil
			},
		},
		TrackMessage: {
			Name: TrackMessage,
			formatFunc: func(message *Message) string {
				td := message.Track
				return fmt.Sprintf("[Track] %s (%s)", td, td.URL)
			},
			typeFunc: func(message *Message) bool {
				for _, ent := range message.Entities {
					trackInfo, err := tracks.GetTrackFromURL(ent.Data)
					if err != nil {
						log.Fatal("[TrackMessage] Error getting track info: ", err)
					}
					if trackInfo != nil {
						message.Track = &TrackData{
							Artists: trackInfo.Artists,
							Title:   trackInfo.Title,
							URL:     trackInfo.URL,
						}
						return true
					}
				}
				return false
			},
		},
	}
)
