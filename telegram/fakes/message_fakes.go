package fakes

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gopkg.in/telegram-bot-api.v4"
)

// FakeIntID is a fake ID
var FakeIntID = 1234567

// FakeInt64ID is a fake ID
var FakeInt64ID = int64(1234567890)

// FakeFileID is a fake file ID
// noinspection SpellCheckingInspection
var FakeFileID = "CAADBAADWQADOXl6AbH307rHZ70EFgQ"

// FakeUser is a fake tgbotapi user
var FakeUser = tgbotapi.User{
	ID:           FakeIntID,
	FirstName:    "Testy",
	LastName:     "McTest",
	UserName:     "Testy",
	LanguageCode: "en-US",
	IsBot:        false,
}

// FakeUnixTimestamp is a fake unix timestamp
var FakeUnixTimestamp = 1498899600 // 07/01/2017 @ 9:00am (UTC)

// FakeGroupChat is a fake tgbotapi group chat
var FakeGroupChat = tgbotapi.Chat{
	ID:                  FakeInt64ID,
	Type:                "group",
	Title:               "fake tgbotapi Group",
	UserName:            "",
	FirstName:           "",
	LastName:            "",
	AllMembersAreAdmins: false,
	Photo:               nil,
	Description:         "",
	InviteLink:          "",
}

// FakePrivateChat is a fake tgbotapi private chat
var FakePrivateChat = tgbotapi.Chat{
	ID:                  FakeInt64ID,
	Type:                "private",
	Title:               "",
	UserName:            FakeUser.UserName,
	FirstName:           FakeUser.FirstName,
	LastName:            FakeUser.LastName,
	AllMembersAreAdmins: false,
	Photo:               nil,
	Description:         "",
	InviteLink:          "",
}

// FakeCommandText is the text to be used with the FakeCommandEntity
var FakeCommandText = "/start"

// FakeCommandEntity is a fake messaging entity as command
var FakeCommandEntity = messaging.Entity{
	MessageEntity: &tgbotapi.MessageEntity{
		Type:   "bot_command",
		Offset: 0,
		Length: 6,
		URL:    "",
		User:   nil,
	},
	Data: "",
}

// FakeThumbnail is a fake tgbotapi thumbnail
var FakeThumbnail = tgbotapi.PhotoSize{
	FileID:   FakeFileID,
	Width:    128,
	Height:   128,
	FileSize: 7884,
}

// FakeSticker is a fake tgbotapi sticker
var FakeSticker = tgbotapi.Sticker{
	FileID:    FakeFileID,
	Width:     512,
	Height:    512,
	Thumbnail: &FakeThumbnail,
	Emoji:     "\u274c",
	FileSize:  48450,
}

// FakePhotoSizeXS is a fake tgbotapi photo size
var FakePhotoSizeXS = tgbotapi.PhotoSize{
	FileID:   FakeFileID,
	Width:    90,
	Height:   61,
	FileSize: 1504,
}

// FakePhotoSizeS is a fake tgbotapi photo size
var FakePhotoSizeS = tgbotapi.PhotoSize{
	FileID:   FakeFileID,
	Width:    320,
	Height:   218,
	FileSize: 19072,
}

// FakePhotoSizeM is a fake tgbotapi photo size
var FakePhotoSizeM = tgbotapi.PhotoSize{
	FileID:   FakeFileID,
	Width:    800,
	Height:   544,
	FileSize: 65830,
}

// FakePhotoSizeL is a fake tgbotapi photo size
var FakePhotoSizeL = tgbotapi.PhotoSize{
	FileID:   FakeFileID,
	Width:    1174,
	Height:   798,
	FileSize: 107605,
}

// FakeDocument is a fake tgbotapi document
var FakeDocument = tgbotapi.Document{
	FileID:    FakeFileID,
	Thumbnail: &FakeThumbnail,
	FileName:  "test.jpg",
	MimeType:  "image/jpeg",
	FileSize:  123456,
}

// FakeAudio is a fake tgbotapi audio
var FakeAudio = tgbotapi.Audio{
	FileID:    FakeFileID,
	Duration:  90,
	Performer: "SomeBody",
	Title:     "SomeThing",
	MimeType:  "audio/mpeg",
	FileSize:  123456,
}

// FakeMessage is a fake tgbotapi message
var FakeMessage = tgbotapi.Message{
	MessageID:             FakeIntID,
	From:                  &FakeUser,
	Date:                  FakeUnixTimestamp,
	Chat:                  nil,
	ForwardFrom:           nil,
	ForwardFromChat:       nil,
	ForwardFromMessageID:  0,
	ForwardDate:           0,
	ReplyToMessage:        nil,
	EditDate:              0,
	Text:                  "",
	Entities:              nil,
	Audio:                 nil,
	Document:              nil,
	Game:                  nil,
	Photo:                 nil,
	Sticker:               nil,
	Video:                 nil,
	VideoNote:             nil,
	Voice:                 nil,
	Caption:               "",
	Contact:               nil,
	Location:              nil,
	Venue:                 nil,
	NewChatMembers:        nil,
	LeftChatMember:        nil,
	NewChatTitle:          "",
	NewChatPhoto:          nil,
	DeleteChatPhoto:       false,
	GroupChatCreated:      false,
	SuperGroupChatCreated: false,
	ChannelChatCreated:    false,
	MigrateToChatID:       0,
	MigrateFromChatID:     0,
	PinnedMessage:         nil,
	Invoice:               nil,
	SuccessfulPayment:     nil,
}

// Message returns a pointer to a copy of msg
func Message(msg tgbotapi.Message) *tgbotapi.Message {
	m := msg
	return &m
}

// Chat returns a pointer to a copy of chat
func Chat(chat tgbotapi.Chat) *tgbotapi.Chat {
	c := chat
	return &c
}

// Sticker returns a pointer to a copy of sticker
func Sticker(sticker tgbotapi.Sticker) *tgbotapi.Sticker {
	c := sticker
	return &c
}

// PhotoSizes returns a pointer to a PhotoSize array
func PhotoSizes() *[]tgbotapi.PhotoSize {
	p := []tgbotapi.PhotoSize{FakePhotoSizeXS, FakePhotoSizeS, FakePhotoSizeM, FakePhotoSizeL}
	return &p
}

// MessageEntities returns a pointer to an array of MessageEntity
func MessageEntities(msg *messaging.Message, entities ...messaging.Entity) {
	var tgEntities []tgbotapi.MessageEntity
	for _, e := range entities {
		msg.Entities = append(msg.Entities, &e)
		tgEntities = append(tgEntities, *e.MessageEntity)
	}
	msg.Message.Entities = &tgEntities
}

// Document returns a pointer to a copy of doc
func Document(doc tgbotapi.Document) *tgbotapi.Document {
	c := doc
	return &c
}

// Audio returns a pointer to a copy of audio
func Audio(audio tgbotapi.Audio) *tgbotapi.Audio {
	c := audio
	return &c
}
