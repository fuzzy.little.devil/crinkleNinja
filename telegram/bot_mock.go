package telegram

import (
	"errors"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"strings"
)

const MockAdminID = 1234567890
const MockBotID = 1234567890
const MockMessageID = 1234567890
const MockBotUserID = "mock_bot"
const MockFileDirectURL = "http://example.com/file"
const MockMessageText = "Test Message"

type MockBot struct {
	Failing bool
}

func (b MockBot) AdminIDs() []int {
	return []int{MockAdminID}
}

func (b MockBot) ID() int {
	return MockBotID
}

func (b MockBot) UserName() string {
	return MockBotUserID
}

func (b MockBot) GetFileDirectURL(string) (string, error) {
	if !b.Failing {
		return MockFileDirectURL, nil
	} else {
		return "", errors.New("test error")
	}
}

func (b MockBot) GetMe() (tgbotapi.User, error) {
	if !b.Failing {
		return tgbotapi.User{
			ID:           MockBotID,
			FirstName:    "",
			LastName:     "",
			UserName:     MockBotUserID,
			LanguageCode: "",
			IsBot:        true,
		}, nil
	} else {
		return tgbotapi.User{}, errors.New("test error")
	}
}

func (b MockBot) IsMessageToMe(message tgbotapi.Message) bool {
	return strings.Contains(message.Text, "@"+b.UserName())
}

func (b MockBot) Send(_ tgbotapi.Chattable) (tgbotapi.Message, error) {
	if !b.Failing {
		return tgbotapi.Message{
			MessageID:             MockMessageID,
			From:                  nil,
			Date:                  0,
			Chat:                  nil,
			ForwardFrom:           nil,
			ForwardFromChat:       nil,
			ForwardFromMessageID:  0,
			ForwardDate:           0,
			ReplyToMessage:        nil,
			EditDate:              0,
			Text:                  MockMessageText,
			Entities:              nil,
			Audio:                 nil,
			Document:              nil,
			Animation:             nil,
			Game:                  nil,
			Photo:                 nil,
			Sticker:               nil,
			Video:                 nil,
			VideoNote:             nil,
			Voice:                 nil,
			Caption:               "",
			Contact:               nil,
			Location:              nil,
			Venue:                 nil,
			NewChatMembers:        nil,
			LeftChatMember:        nil,
			NewChatTitle:          "",
			NewChatPhoto:          nil,
			DeleteChatPhoto:       false,
			GroupChatCreated:      false,
			SuperGroupChatCreated: false,
			ChannelChatCreated:    false,
			MigrateToChatID:       0,
			MigrateFromChatID:     0,
			PinnedMessage:         nil,
			Invoice:               nil,
			SuccessfulPayment:     nil,
			PassportData:          nil,
		}, nil
	} else {
		return tgbotapi.Message{}, errors.New("test error")
	}
}

func (b MockBot) DeleteMessage(_ tgbotapi.DeleteMessageConfig) (tgbotapi.APIResponse, error) {
	if !b.Failing {
		return tgbotapi.APIResponse{}, nil
	} else {
		return tgbotapi.APIResponse{}, errors.New("test error")
	}
}

func (b MockBot) GetWebhookInfo() (tgbotapi.WebhookInfo, error) {
	if !b.Failing {
		return tgbotapi.WebhookInfo{}, nil
	} else {
		return tgbotapi.WebhookInfo{}, errors.New("test error")
	}
}
