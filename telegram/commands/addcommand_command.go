package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
)

func addCommandCmdStart(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) {
	txt := "Send me your code! Use /stop to end."
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	data := util.InterfaceToMap(ea.Data.Data)
	if data == nil {
		data = map[string]string{}
	}
	data["name"] = message.CommandArguments()
	data["script"] = ""
	ea.Data.Data = data
	ea.Data.Save()
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
	}
	ea.UpdateContext(model.CurrentUser)
}

func addCommandCmd(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if !ea.HasCurrentContext(model.CurrentUser) {
		if message.CommandArguments() == "" {
			_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, "Syntax: /addcommand <command>"))
			return nil
		}
		if IsBuiltInCommand(message.CommandArguments()) {
			_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, "Cannot override built-in command."))
			return nil
		}
		addCommandCmdStart(ea, message, bot)
		return nil
	}
	data := util.InterfaceToMap(ea.Data.Data)
	if data == nil {
		log.Printf("Unable to fetch event data")
		return nil
	}
	ea.Data.Data = map[string]string{}
	ea.Data.Save()
	c := model.NewCommand(data["name"], message.Text)
	c.Save()
	_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, "Command created!"))
	ea.ClearContext(model.CurrentUser)
	return nil
}

var AddCmdCommand = &events.EventAction{
	Name:          "addcommand",
	Help:          "Allows sending lua code for a new command!",
	IsInStartList: false,
	HasData:       true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    addCommandCmd,
}
