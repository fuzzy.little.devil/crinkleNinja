package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"log"
)

func infoCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	text := "Your Telegram Information:\n"
	text += "First Name: " + message.From.FirstName + "\n"
	text += "Last Name: " + message.From.LastName + "\n"
	text += "Username: " + message.From.UserName + "\n"
	text += fmt.Sprintf("Telegram ID: %d\n", message.From.ID)
	text += "\nChat Information: \n"
	text += "Chat First Name: " + message.Chat.FirstName + "\n"
	text += "Chat Last Name: " + message.Chat.LastName + "\n"
	text += "Chat Username: " + message.Chat.UserName + "\n"
	text += "Chat Title: " + message.Chat.Title + "\n"
	text += "Chat Type: " + message.Chat.Type + "\n"
	text += "Chat Description: " + message.Chat.Description + "\n"
	text += fmt.Sprintf("Chat Invite Link: %s\n", message.Chat.InviteLink)
	text += fmt.Sprintf("Chat ID: %d\n", message.Chat.ID)
	msg := telegram.NewMessage(message.Message.Chat.ID, text)
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

var InfoCommand = &events.EventAction{
	Name:          "info",
	IsInStartList: true,
	Help:          "Displays your Telegram info, such as ID",
	MatchFuncs:    []events.MatchFunc{IsCommandMatch},
	ActionFunc:    infoCmd,
}
