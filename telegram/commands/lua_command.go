package commands

import (
	"context"
	"fmt"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/lua_api"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
	"time"
)

func luaCmdStart(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) {
	txt := "Send me your code! Use /stop to end."
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	data := util.InterfaceToMap(ea.Data.Data)
	if data == nil {
		data = map[string]string{}
	}
	data[message.From.UserName] = ""
	ea.Data.Data = data
	ea.Data.Save()
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
	}
	ea.UpdateContext(model.CurrentUser)
}

func luaCmd(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if !ea.HasCurrentContext(model.CurrentUser) {
		luaCmdStart(ea, message, bot)
		return nil
	}
	data := util.InterfaceToMap(ea.Data.Data)
	if data == nil {
		log.Printf("Unable to fetch event data")
		return nil
	}
	script := data[message.From.UserName]
	data[message.From.UserName] = fmt.Sprintf("%s%s\n", script, message.Text)
	ea.Data.Data = data
	ea.Data.Save()
	return nil
}

func luaCmdStop(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	data := util.InterfaceToMap(ea.Data.Data)
	if data == nil {
		log.Printf("Unable to fetch event data")
		return nil
	}
	script := data[message.From.UserName]
	delete(data, message.From.UserName)
	if script == "" {
		return nil
	}
	ea.Data.Data = data
	ea.Data.Save()
	luaState := lua.NewState()
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	luaConfig := &embedded.LuaConfig{
		Script: script,
		Globals: map[string]embedded.LuaValueFunc{
			"event":   lua_api.AsLuaType(ea, lua_api.EventActionLuaTypeName),
			"message": lua_api.AsLuaType(message, lua_api.MessageLuaTypeName),
			"bot":     lua_api.AsLuaType(bot, lua_api.BotLuaTypeName),
			"chat":    lua_api.AsLuaType(model.CurrentChat, lua_api.ChatLuaTypeName),
			"user":    lua_api.AsLuaType(model.CurrentUser, lua_api.UserLuaTypeName),
		},
		State:      luaState,
		Types:      lua_api.TypeRegistry,
		Modules:    lua_api.ModuleRegistry,
		Context:    ctx,
		CancelFunc: cancel,
	}
	output, err := embedded.RunLua(luaConfig)
	if err != nil {
		_, _ = bot.Send(telegram.NewMessage(message.Message.Chat.ID, err.Error()))
	} else {
		_, _ = bot.Send(telegram.NewMessage(message.Message.Chat.ID, output))
	}
	ea.ClearContext(model.CurrentUser)
	return nil
}

var LuaCommand = &events.EventAction{
	Name:          "lua",
	Help:          "Allows sending lua code for parsing!",
	IsInStartList: false,
	HasData:       true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    luaCmd,
}
