package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func deleteCommandCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	cmd := model.GetCommand(message.CommandArguments())
	if cmd == nil {
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, fmt.Sprintf("Command '%s' not found.",
			message.CommandArguments())))
		return nil
	}
	cmd.Delete()
	text := fmt.Sprintf("Command: %s (%s)\nScript:\n```\n%s\n```\n\nDeleted.", cmd.Name, cmd.Help, cmd.Script)
	msg := telegram.NewMessage(message.Chat.ID, text)
	msg.ParseMode = "Markdown"
	_, _ = bot.Send(msg)
	return nil
}

var DeleteCmdCommand = &events.EventAction{
	Name:          "deletecommand",
	Help:          "Removes the given lua command.",
	IsInStartList: false,
	HasData:       false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    deleteCommandCmd,
}
