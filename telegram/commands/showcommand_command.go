package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func showCommandCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if message.CommandArguments() == "" {
		text := ""
		for _, cmd := range model.GetAllCommands() {
			text += fmt.Sprintf("/%s\n", cmd.Name)
		}
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, text))
		return nil
	}
	cmd := model.GetCommand(message.CommandArguments())
	if cmd == nil {
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, fmt.Sprintf("Command '%s' not found.",
			message.CommandArguments())))
		return nil
	}
	text := "```\n" + cmd.Script + "\n```"
	msg := telegram.NewMessage(message.Chat.ID, text)
	msg.ParseMode = "Markdown"
	_, _ = bot.Send(msg)
	return nil
}

var ShowCmdCommand = &events.EventAction{
	Name:          "showcommand",
	Help:          "Shows the lua code for a given command.",
	IsInStartList: false,
	HasData:       false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    showCommandCmd,
}
