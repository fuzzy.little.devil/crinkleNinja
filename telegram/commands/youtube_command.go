package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/integrations/google"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"log"
)

func youTubeCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if len(message.CommandArguments()) == 0 {
		msg := telegram.NewMessage(message.Message.Chat.ID, "What should I search for?")
		if _, err := bot.Send(msg); err != nil {
			log.Printf("Error sending: %v", err)
		}
		return nil
	}
	result := google.FirstYouTubeResultFor(message.CommandArguments())
	msg := telegram.NewMessage(message.Message.Chat.ID, result)
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

var YouTubeCommand = &events.EventAction{
	Name:          "youtube",
	Help:          "Searches YouTube for the given string and returns the first video found.",
	IsInStartList: true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch},
	ActionFunc:    youTubeCmd,
}
