package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
)

func clearCacheCmd(_ *events.EventAction, _ *messaging.Message, _ telegram.Bottable) error {
	entities.ClearCache()
	return nil
}

var ClearCacheCommand = &events.EventAction{
	Name:          "clearcache",
	IsInStartList: false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    clearCacheCmd,
}
