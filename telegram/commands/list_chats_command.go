package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func listChatsCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	var replies []telegram.MessageConfig
	replies = append(replies, telegram.NewMessage(message.Message.Chat.ID, "Chats in memory:"))
	for _, c := range model.GetAllChats() {
		txt := fmt.Sprintf("Name: %s\n", c.EntityName())
		txt += "Admins: \n"
		if c.AllMembersAreAdmins {
			txt += "\tAll members are admins.\n"
		} else {
			for id, u := range c.Admins {
				txt += fmt.Sprintf("%s [%d]\n", u, id)
			}
		}
		replies = append(replies, telegram.NewMessage(message.Message.Chat.ID, txt))
	}
	for _, r := range replies {
		_, _ = bot.Send(r)
	}
	return nil
}

var ListChatsCommand = &events.EventAction{
	Name:          "listchats",
	Help:          "Lists the chats the bot knows about.",
	IsInStartList: false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch},
	ActionFunc:    listChatsCmd,
}
