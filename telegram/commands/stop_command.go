package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func stopCmd(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if ListenCommand.HasCurrentContext(model.CurrentUser) {
		return listenCmdStop(ListenCommand, message, bot)
	} else if LuaCommand.HasCurrentContext(model.CurrentUser) {
		return luaCmdStop(LuaCommand, message, bot)
	} else if CustomCommand.HasCurrentContext(model.CurrentUser) {
		CustomCommand.ClearContext(model.CurrentUser)
		return customCmd(CustomCommand, message, bot)
	}
	ea.ClearAllContext(model.CurrentUser)
	_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, "Context stopped."))
	return nil
}

var StopCommand = &events.EventAction{
	Name:          "stop",
	Help:          "Ends the current context.",
	IsInStartList: true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch},
	ActionFunc:    stopCmd,
}
