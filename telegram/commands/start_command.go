package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"log"
)

func startCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	text := "Welcome! You can use the following commands:\n"
	for _, cmd := range events.CommandEvent.Actions {
		if cmd.IsInStartList {
			text += fmt.Sprintf("/%s - %s\n", cmd.Name, cmd.Help)
		}
	}
	for _, cmd := range model.GetAllCommands() {
		if cmd.Help != "" {
			text += fmt.Sprintf("/%s - %s\n", cmd.Name, cmd.Help)
		}
	}
	msg := telegram.NewMessage(message.Message.Chat.ID, text)
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

var StartCommand = &events.EventAction{
	Name:          "start",
	Help:          "Prints command list",
	IsInStartList: false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch},
	ActionFunc:    startCmd,
}
