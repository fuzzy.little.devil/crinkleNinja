package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"strings"
)

func setTriggerHelpCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	args := strings.Split(message.CommandArguments(), " ")
	name := args[0]
	helpText := strings.Join(args[1:], " ")
	trigger := model.GetTrigger(name)
	if trigger == nil {
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, fmt.Sprintf("Trigger '%s' not found.",
			message.CommandArguments())))
		return nil
	}
	trigger.Help = helpText
	trigger.Save()
	text := fmt.Sprintf("Trigger '%s' help text set to: '%s'", trigger.Name, trigger.Help)
	_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, text))
	return nil
}

var SetTriggerHelpCommand = &events.EventAction{
	Name:          "settriggerhelp",
	Help:          "Sets the help text for the given trigger.",
	IsInStartList: false,
	HasData:       false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    setTriggerHelpCmd,
}
