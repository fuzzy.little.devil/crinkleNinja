package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"gopkg.in/Knetic/govaluate.v2"
	"log"
	"math/rand"
	"regexp"
	"strconv"
)

var re = regexp.MustCompile(`(?m)^(?P<num>\d+)d(?P<sides>\d+)(?P<mod>[\d+\-*\\]*)$`)

const maxNumDice = 100
const maxDiceSides = 1000

type diceRoll struct {
	Number     int
	Sides      int
	Expression *govaluate.EvaluableExpression
	Rolls      []int
	Roll       int
	Result     string
	Err        error
}

func (d *diceRoll) RollDice() {
	if d.Number <= 0 || d.Number > maxNumDice {
		d.Err = fmt.Errorf("dice count must be between 1 and %d", maxNumDice)
		return
	}
	if d.Sides <= 1 || d.Sides > maxDiceSides {
		d.Err = fmt.Errorf("dice sides must be between 2 and %d", maxDiceSides)
		return
	}
	for i := 0; i < d.Number; i++ {
		r := rand.Intn(d.Sides) + 1
		d.Rolls = append(d.Rolls, r)
		d.Roll += r
	}
}

func (d *diceRoll) Evaluate() {
	if d.Expression == nil || d.Err != nil {
		return
	}
	params := make(map[string]interface{})
	params["roll"] = d.Roll
	result, err := d.Expression.Evaluate(params)
	if err != nil {
		d.Err = err
	}
	d.Result = fmt.Sprintf("%v", result)
}

func (d *diceRoll) Text(actor string) string {
	text := ""
	if d.Err != nil {
		return d.Err.Error()
	}
	if d.Roll == 0 {
		return fmt.Sprintf("%s rolled nothing. :(", actor)
	} else if d.Number == 1 {
		if d.Sides == 2 && d.Expression == nil {
			text = fmt.Sprintf("%s flipped a coin and it landed on %s.", actor, headsOrTails(d.Roll))
		} else {
			text = fmt.Sprintf("%s rolled a %d-sided die which came up %d.", actor, d.Sides, d.Roll)
		}
	} else {
		if d.Sides == 2 && d.Expression == nil {
			coins := ""
			for _, roll := range d.Rolls {
				coins += headsOrTails(roll) + ", "
			}
			coins = coins[:len(coins)-2]
			text = fmt.Sprintf("%s tossed %d coins into the air! They landed on %s.", actor, d.Number, coins)
		} else {
			rolls := "("
			for _, roll := range d.Rolls {
				rolls += strconv.Itoa(roll) + ", "
			}
			rolls = rolls[:len(rolls)-2] + ")"
			text = fmt.Sprintf("%s rolled %d %d-sided dice %s which totals %d.", actor, d.Number, d.Sides, rolls, d.Roll)
		}
	}
	if d.Expression != nil && d.Roll != 0 {
		text += fmt.Sprintf(" With modifiers, the result is %s.", d.Result)
	}
	return text
}

func newDiceRoll(expression string) (*diceRoll, error) {
	d := new(diceRoll)
	if !re.MatchString(expression) {
		return nil, fmt.Errorf("invalid dice roll")
	}
	expr := util.GetRegexGroups(expression, re)
	if len(expr) < 3 {
		return nil, fmt.Errorf("invalid dice roll")
	}
	var err error
	d.Number, err = strconv.Atoi(expr["num"])
	if err != nil {
		return nil, err
	}
	d.Sides, err = strconv.Atoi(expr["sides"])
	if err != nil {
		return nil, err
	}
	if expr["mod"] != "" {
		d.Expression, err = govaluate.NewEvaluableExpression("[roll]" + expr["mod"])
		if err != nil {
			return nil, err
		}
	}
	return d, nil
}

func headsOrTails(roll int) string {
	if roll == 1 {
		return "tails"
	}
	return "heads"
}

func rollCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	r, err := newDiceRoll(message.CommandArguments())
	var text = ""
	if err != nil {
		text = err.Error()
	} else {
		r.RollDice()
		r.Evaluate()
		text = r.Text(message.From.FirstName)
	}
	msg := telegram.NewMessage(message.Message.Chat.ID, text)
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

func flipCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	r := new(diceRoll)
	r.Number = 1
	r.Sides = 2
	r.RollDice()
	msg := telegram.NewMessage(message.Message.Chat.ID, r.Text(message.From.FirstName))
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

var RollCommand = &events.EventAction{
	Name:          "roll",
	Help:          "Rolls dice. Supports xdx+mod. Example: 2d20+10 or 1d6-1",
	IsInStartList: true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch},
	ActionFunc:    rollCmd,
}

var FlipCommand = &events.EventAction{
	Name:       "flip",
	MatchFuncs: []events.MatchFunc{IsCommandMatch},
	ActionFunc: flipCmd,
}
