package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/integrations/xkcd"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"log"
	"mime"
	"strconv"
	"strings"
)

func postStaticImage(strip *xkcd.Strip, image *xkcd.ImageData, message *messaging.Message, bot telegram.Bottable) {
	ext, err := mime.ExtensionsByType(image.ContentType)
	if err != nil {
		xkcdError(err, message, bot)
		return
	}
	if ext == nil || len(ext) == 0 {
		postLink(strip, message, bot)
		return
	}
	file := tgbotapi.FileBytes{Name: fmt.Sprintf("xkcd_%d.%s", strip.Num, ext), Bytes: image.Bytes}
	imgUpload := tgbotapi.NewPhotoUpload(message.Chat.ID, file)
	imgUpload.Caption = fmt.Sprintf("(%s) %s", strip.Date().Format("2006-1-2"), strip.Alt)
	if _, err := bot.Send(imgUpload); err != nil {
		log.Printf("Error sending: %v", err)
	}
}

func postAnimatedImage(strip *xkcd.Strip, image *xkcd.ImageData, message *messaging.Message, bot telegram.Bottable) {
	file := tgbotapi.FileBytes{Name: fmt.Sprintf("xkcd_%d.gif", strip.Num), Bytes: image.Bytes}
	imgUpload := tgbotapi.NewAnimationUpload(message.Chat.ID, file)
	imgUpload.Caption = fmt.Sprintf("(%s) %s", strip.Date().Format("2006-1-2"), strip.Alt)
	if _, err := bot.Send(imgUpload); err != nil {
		log.Printf("Error sending: %v", err)
	}
}

func postLink(strip *xkcd.Strip, message *messaging.Message, bot telegram.Bottable) {
	msg := telegram.NewMessage(message.Chat.ID,
		fmt.Sprintf("I can't display this one, so here's the link! https://xkcd.com/%d", strip.Num))
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
}

func postXKCDComic(strip *xkcd.Strip, message *messaging.Message, bot telegram.Bottable) {
	i, err := strip.ImageData()
	if err != nil {
		xkcdError(err, message, bot)
		return
	}
	if strip.ExtraParts == nil {
		if i.ContentType == "image/jpeg" || i.ContentType == "image/png" {
			postStaticImage(strip, i, message, bot)
			return
		}
		if i.ContentType == "image/gif" {
			postAnimatedImage(strip, i, message, bot)
			return
		}
	}
	postLink(strip, message, bot)
}

func xkcdError(err error, message *messaging.Message, bot telegram.Bottable) {
	log.Printf("XKCD Error: %v", err)
	msg := telegram.NewMessage(message.Message.Chat.ID,
		"I either couldn't find that comic or couldn't reach the website.")
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
}

func xkcdCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	num, err := strconv.Atoi(message.CommandArguments())
	if err == nil {
		s, err := xkcd.GetStripNumber(num)
		if err != nil {
			xkcdError(err, message, bot)
			return nil
		}
		postXKCDComic(s, message, bot)
		return nil
	}
	if strings.ToLower(message.CommandArguments()) == "latest" {
		s, err := xkcd.GetLatestStrip()
		if err != nil {
			xkcdError(err, message, bot)
			return nil
		}
		postXKCDComic(s, message, bot)
		return nil
	}
	s, err := xkcd.GetRandomStrip()
	if err != nil {
		xkcdError(err, message, bot)
		return nil
	}
	postXKCDComic(s, message, bot)
	return nil
}

var XKCDCommand = &events.EventAction{
	Name:          "xkcd",
	Help:          "Shows a random, latest or specified comic strip",
	IsInStartList: true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch},
	ActionFunc:    xkcdCmd,
}
