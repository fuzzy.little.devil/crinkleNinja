package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func showTriggerCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if message.CommandArguments() == "" {
		text := ""
		for _, trigger := range model.GetAllTriggers() {
			text += fmt.Sprintf("/%s\n", trigger.Name)
		}
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, text))
		return nil
	}
	trigger := model.GetTrigger(message.CommandArguments())
	if trigger == nil {
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, fmt.Sprintf("Trigger '%s' not found.",
			message.CommandArguments())))
		return nil
	}
	text := fmt.Sprintf("Match:\n```%s\n```\nScript:\n```%s\n```", trigger.MatchScript, trigger.Script)
	msg := telegram.NewMessage(message.Chat.ID, text)
	msg.ParseMode = "Markdown"
	_, _ = bot.Send(msg)
	return nil
}

var ShowTriggerCommand = &events.EventAction{
	Name:          "showtrigger",
	Help:          "Shows the lua code for a given trigger name.",
	IsInStartList: false,
	HasData:       false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    showTriggerCmd,
}
