package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"strings"
)

func setCommandHelpCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	args := strings.Split(message.CommandArguments(), " ")
	cmdName := args[0]
	helpText := strings.Join(args[1:], " ")
	cmd := model.GetCommand(cmdName)
	if cmd == nil {
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, fmt.Sprintf("Command '%s' not found.",
			message.CommandArguments())))
		return nil
	}
	cmd.Help = helpText
	cmd.Save()
	text := fmt.Sprintf("/%s help text set to: '%s'", cmd.Name, cmd.Help)
	_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, text))
	return nil
}

var SetCommandHelpCommand = &events.EventAction{
	Name:          "setcommandhelp",
	Help:          "Sets the help text for the given command.",
	IsInStartList: false,
	HasData:       false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    setCommandHelpCmd,
}
