package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"log"
	"math/rand"
)

var randomReplies = []string{
	"Cool",
	"Nice story",
	"You're annoying",
	"Don't you have anything better to be doing?",
	"You're a talkative baby.",
}

func listenCmdStart(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) {
	txt := "I'm listening. Use /stop to end."
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
	}
	ea.UpdateContext(model.CurrentUser)
}

func listenCmd(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if !ea.HasCurrentContext(model.CurrentUser) {
		listenCmdStart(ea, message, bot)
		return nil
	}
	txt := randomReplies[rand.Intn(len(randomReplies))]
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

func listenCmdStop(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	txt := "Okay, I'm done listening anyway."
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
	}
	ea.ClearContext(model.CurrentUser)
	return nil
}

var ListenCommand = &events.EventAction{
	Name:          "listen",
	Help:          "Asks the bot to listen to you. Warning - it's a bit cranky.",
	IsInStartList: true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch},
	ActionFunc:    listenCmd,
}
