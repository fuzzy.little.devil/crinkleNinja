package commands

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func deleteTriggerCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	trigger := model.GetTrigger(message.CommandArguments())
	if trigger == nil {
		_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, fmt.Sprintf("Trigger '%s' not found.",
			message.CommandArguments())))
		return nil
	}
	trigger.Delete()
	text := fmt.Sprintf("Trigger: %s (%s)\nMatch:\n```%s```\nScript:\n```\n%s\n```\n\nDeleted.", trigger.Name,
		trigger.Help, trigger.MatchScript, trigger.Script)
	msg := telegram.NewMessage(message.Chat.ID, text)
	msg.ParseMode = "Markdown"
	_, _ = bot.Send(msg)
	return nil
}

var DeleteTriggerCommand = &events.EventAction{
	Name:          "deletetrigger",
	Help:          "Removes the given lua trigger.",
	IsInStartList: false,
	HasData:       false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch, IsAdminMatch},
	ActionFunc:    deleteTriggerCmd,
}
