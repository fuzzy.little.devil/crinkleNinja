package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func cancelCmd(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	ea.ClearAllContext(model.CurrentUser)
	_, _ = bot.Send(telegram.NewMessage(message.Chat.ID, "Context cancelled."))
	return nil
}

var CancelCommand = &events.EventAction{
	Name:          "cancel",
	Help:          "Ends the current context without additional handling.",
	IsInStartList: true,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch},
	ActionFunc:    cancelCmd,
}
