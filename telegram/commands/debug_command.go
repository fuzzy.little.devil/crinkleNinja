package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/keyboards"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

func debugCmd(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	msg := telegram.NewMessage(message.Chat.ID, message.Message.Text)
	msg.ReplyMarkup = keyboards.DebugKeyboard.API()
	keyboards.DebugKeyboard.EventAction.UpdateContext(model.CurrentUser)
	model.CurrentUser.Save()
	_, _ = bot.Send(msg)
	return nil
}

var DebugCommand = &events.EventAction{
	Name:          "debug",
	Help:          "Enters a debugging context",
	IsInStartList: false,
	MatchFuncs:    []events.MatchFunc{IsCommandMatch, IsPrivateMatch},
	ActionFunc:    debugCmd,
}
