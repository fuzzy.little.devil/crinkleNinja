package commands

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
	"strings"
)

var commandsLoaded = false

func InitCommands() {
	if commandsLoaded {
		return
	}
	events.CommandEvent.AddAction(StartCommand)
	events.CommandEvent.AddAction(DebugCommand)
	events.CommandEvent.AddAction(ListChatsCommand)
	events.CommandEvent.AddAction(ListenCommand)
	events.CommandEvent.AddAction(StopCommand)
	events.CommandEvent.AddAction(CancelCommand)
	events.CommandEvent.AddAction(RollCommand)
	events.CommandEvent.AddAction(FlipCommand)
	events.CommandEvent.AddAction(InfoCommand)
	events.CommandEvent.AddAction(LuaCommand)
	events.CommandEvent.AddAction(AddCmdCommand)
	events.CommandEvent.AddAction(ShowCmdCommand)
	events.CommandEvent.AddAction(SetCommandHelpCommand)
	events.CommandEvent.AddAction(DeleteCmdCommand)
	events.CommandEvent.AddAction(CustomCommand)
	events.CommandEvent.AddAction(AddTriggerCommand)
	events.CommandEvent.AddAction(ShowTriggerCommand)
	events.CommandEvent.AddAction(DeleteTriggerCommand)
	events.CommandEvent.AddAction(SetTriggerHelpCommand)
	events.CommandEvent.AddAction(XKCDCommand)
	events.CommandEvent.AddAction(ClearCacheCommand)
	events.CommandEvent.AddAction(YouTubeCommand)
	commandsLoaded = true
}

func IsCommandMatch(ea *events.EventAction, message *messaging.Message, _ telegram.Bottable) bool {
	return message.IsCommand() && strings.ToLower(message.Command()) == strings.ToLower(ea.Name)
}

func IsPrivateMatch(_ *events.EventAction, message *messaging.Message, _ telegram.Bottable) bool {
	return message.Chat.IsPrivate()
}

func IsAdminMatch(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) bool {
	return util.InSlice(message.From.ID, bot.AdminIDs())
}

func IsBuiltInCommand(commandName string) bool {
	for name := range events.CommandEvent.Actions {
		if name == strings.Trim(commandName, "/") {
			return true
		}
	}
	return false
}

func CheckError(err error, bot telegram.Bottable, chatID int64) bool {
	if err != nil {
		reply := telegram.NewMessage(chatID, err.Error())
		if _, err := bot.Send(reply); err != nil {
			log.Printf("Error sending: %v", err)
		}
		return true
	}
	return false
}
