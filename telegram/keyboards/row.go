package keyboards

import tgbotapi "gopkg.in/telegram-bot-api.v4"

type Row struct {
	Buttons map[string]*Button
}

func (r *Row) API() []tgbotapi.KeyboardButton {
	var buttons []tgbotapi.KeyboardButton
	for _, b := range r.Buttons {
		buttons = append(buttons, b.API())
	}
	return tgbotapi.NewKeyboardButtonRow(buttons...)
}

func (r *Row) AddButton(name string, action Action) {
	if r.Buttons == nil {
		r.Buttons = map[string]*Button{}
	}
	r.Buttons[name] = &Button{Name: &name, Action: action}
}
