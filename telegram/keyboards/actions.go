package keyboards

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"log"
	"os"
	"time"
)

type Action func(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error

func closeAction(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	msg := telegram.NewMessage(message.Chat.ID, message.Message.Text)
	msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	_, _ = bot.Send(msg)
	ea.ClearContext(model.CurrentUser)
	return nil
}

func debugInfoAction(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	txt := fmt.Sprintf("My name: %s\nStage: %s\nDebug mode: %v\n", bot.UserName(), config.Stage, config.IsDebug())
	txt += fmt.Sprintf("Lambda Info\nRegion: %s\nFunction Name: %s\nFunction Version: %s\nMemory Size: %s\n",
		os.Getenv("AWS_REGION"), os.Getenv("AWS_LAMBDA_FUNCTION_NAME"),
		os.Getenv("AWS_LAMBDA_FUNCTION_VERSION"), os.Getenv("AWS_LAMBDA_FUNCTION_MEMORY_SIZE"))
	info, err := bot.GetWebhookInfo()
	if err != nil {
		txt += "\nError getting web hook info: " + err.Error()
	} else {
		txt += fmt.Sprintf("\nWebhook Info\nURL: %s\nPending Updates: %d\nLast Error: %s\nLast Error Message: %s\n",
			info.URL, info.PendingUpdateCount, time.Unix(int64(info.LastErrorDate), 0).Format(time.RFC822Z), info.LastErrorMessage)
	}
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	reply.ReplyToMessageID = message.Message.MessageID
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
		return err
	}
	return nil
}
