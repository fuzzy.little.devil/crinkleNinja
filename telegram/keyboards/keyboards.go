package keyboards

import "gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"

var replyKeyboards = map[string]*ReplyKeyboard{}

var keyboardsLoaded = false

func InitKeyboards() {
	if keyboardsLoaded {
		return
	}
	AddReplyKeyboard(DebugKeyboard)
	keyboardsLoaded = true
}

func AddReplyKeyboard(keyboard *ReplyKeyboard) {
	replyKeyboards[*keyboard.Name] = keyboard
}

func CreateKeyboard(name string) *ReplyKeyboard {
	kb := &ReplyKeyboard{
		Name: &name,
		EventAction: &events.EventAction{
			Name:        name,
			MatchFuncs:  []events.MatchFunc{KeyboardMatch},
			ActionFunc:  KeyboardHandler,
			IsExclusive: true,
		},
	}
	events.ReplyKeyboardEvent.AddAction(kb.EventAction)
	return kb
}

var DebugKeyboard = func() *ReplyKeyboard {
	kb := CreateKeyboard("debug")
	row := &Row{}
	row.AddButton("Info", debugInfoAction)
	row.AddButton("Close", closeAction)
	kb.AddRow(row)
	return kb
}()
