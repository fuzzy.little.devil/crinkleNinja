package keyboards

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

type ReplyKeyboard struct {
	Name        *string
	Rows        []*Row
	EventAction *events.EventAction
}

func (k *ReplyKeyboard) AddRow(row *Row) {
	k.Rows = append(k.Rows, row)
}

func (k *ReplyKeyboard) API() tgbotapi.ReplyKeyboardMarkup {
	var rows [][]tgbotapi.KeyboardButton
	for _, r := range k.Rows {
		rows = append(rows, r.API())
	}
	return tgbotapi.NewReplyKeyboard(rows...)
}

func KeyboardMatch(ea *events.EventAction, _ *messaging.Message, _ telegram.Bottable) bool {
	return ea.HasCurrentContext(model.CurrentUser)
}

func KeyboardHandler(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	if !ea.HasCurrentContext(model.CurrentUser) {
		return nil
	}
	kb, ok := replyKeyboards[ea.Name]
	if !ok {
		fmt.Printf("could not find active keyboard: %s", ea.Name)
		ea.ClearContext(model.CurrentUser)
		return nil
	}
	for _, r := range kb.Rows {
		for _, b := range r.Buttons {
			if *b.Name == message.Text {
				err := b.Action(ea, message, bot)
				return err
			}
		}
	}
	return nil
}
