package keyboards

import (
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

type Button struct {
	Name   *string
	Action Action
}

func (b *Button) API() tgbotapi.KeyboardButton {
	return tgbotapi.NewKeyboardButton(*b.Name)
}
