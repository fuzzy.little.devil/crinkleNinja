package triggers

import (
	"context"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/lua_api"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"log"
	"time"
)

func customTriggerMatch(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) bool {
	triggers := model.GetAllTriggers()
	for _, trigger := range triggers {
		luaState := lua.NewState()
		ctx, cancel := context.WithTimeout(context.Background(), 250*time.Millisecond)
		luaConfig := &embedded.LuaConfig{
			Script: trigger.MatchScript,
			Globals: map[string]embedded.LuaValueFunc{
				"event":   lua_api.AsLuaType(ea, lua_api.EventActionLuaTypeName),
				"message": lua_api.AsLuaType(message, lua_api.MessageLuaTypeName),
				"bot":     lua_api.AsLuaType(bot, lua_api.BotLuaTypeName),
				"chat":    lua_api.AsLuaType(model.CurrentChat, lua_api.ChatLuaTypeName),
				"user":    lua_api.AsLuaType(model.CurrentUser, lua_api.UserLuaTypeName),
			},
			State:      luaState,
			Modules:    lua_api.ModuleRegistry,
			Types:      lua_api.TypeRegistry,
			Context:    ctx,
			CancelFunc: cancel,
		}
		_, err := embedded.RunLua(luaConfig)
		if err != nil {
			_, _ = bot.Send(telegram.NewMessage(message.Message.Chat.ID, err.Error()))
			break
		}
		if lua.LVAsBool(luaState.Get(1)) {
			ea.Data = model.NewEventActionData(ea.Name)
			ea.Data.Data = trigger
			return true
		}
	}
	return false
}

func customTriggerAction(ea *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	trigger, ok := ea.Data.Data.(*model.Trigger)
	if !ok {
		log.Println("error extracting custom trigger")
		return nil
	}
	luaState := lua.NewState()
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	luaConfig := &embedded.LuaConfig{
		Script: trigger.Script,
		Globals: map[string]embedded.LuaValueFunc{
			"event":   lua_api.AsLuaType(ea, lua_api.EventActionLuaTypeName),
			"message": lua_api.AsLuaType(message, lua_api.MessageLuaTypeName),
			"bot":     lua_api.AsLuaType(bot, lua_api.BotLuaTypeName),
			"chat":    lua_api.AsLuaType(model.CurrentChat, lua_api.ChatLuaTypeName),
			"user":    lua_api.AsLuaType(model.CurrentUser, lua_api.UserLuaTypeName),
		},
		State:      luaState,
		Modules:    lua_api.ModuleRegistry,
		Types:      lua_api.TypeRegistry,
		Context:    ctx,
		CancelFunc: cancel,
	}
	output, err := embedded.RunLua(luaConfig)
	if err != nil {
		_, _ = bot.Send(telegram.NewMessage(message.Message.Chat.ID, err.Error()))
	} else {
		_, _ = bot.Send(telegram.NewMessage(message.Message.Chat.ID, output))
	}
	return nil
}

var CustomTrigger = &events.EventAction{
	Name:       "CustomTrigger",
	HasData:    false,
	MatchFuncs: []events.MatchFunc{customTriggerMatch},
	ActionFunc: customTriggerAction,
}
