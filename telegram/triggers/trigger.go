package triggers

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
)

var triggersLoaded = false

// For now, add triggers via init
func InitTriggers() {
	if triggersLoaded {
		return
	}
	events.TriggerEvent.AddAction(TrackTrigger)
	events.TriggerEvent.AddAction(MediaIDTrigger)
	events.TriggerEvent.AddAction(CustomTrigger)
	triggersLoaded = true
}
