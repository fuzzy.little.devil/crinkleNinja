package triggers

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/integrations/google"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"log"
	"strings"
)

func trackTriggerMatch(_ *events.EventAction, message *messaging.Message, _ telegram.Bottable) bool {
	return message.MessageType().Name == messaging.TrackMessage
}

func trackTriggerAction(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	result := google.FirstYouTubeResultFor(fmt.Sprintf("%s official", message.Track))
	txt := fmt.Sprintf("Sorry boss, didn't find anything for %s.", message.Track)
	if len(result) == 0 {
		log.Printf("No result from YouTube for Artist: %s, Song: %s", strings.Join(message.Track.Artists, ", "), message.Track.Title)
	} else {
		txt = fmt.Sprintf("Found a result for %s on YouTube: %s", message.Track, result)
	}
	reply := telegram.NewMessage(message.Message.Chat.ID, txt)
	reply.ReplyToMessageID = message.Message.MessageID
	if _, err := bot.Send(reply); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

var TrackTrigger = &events.EventAction{
	Name:       "TrackTrigger",
	MatchFuncs: []events.MatchFunc{trackTriggerMatch},
	ActionFunc: trackTriggerAction,
}
