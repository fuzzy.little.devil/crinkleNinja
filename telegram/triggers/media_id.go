package triggers

import (
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"log"
)

func mediaIDTriggerMatch(_ *events.EventAction, message *messaging.Message, _ telegram.Bottable) bool {
	return message.Chat.IsPrivate() && (message.Sticker != nil || message.Animation != nil || message.Video != nil ||
		message.Photo != nil)
}

func mediaIDTriggerAction(_ *events.EventAction, message *messaging.Message, bot telegram.Bottable) error {
	txt := "No ID"
	if message.Sticker != nil {
		txt = fmt.Sprintf("Sticker ID: %s", message.Sticker.FileID)
	} else if message.Animation != nil {
		txt = fmt.Sprintf("Animation ID: %s", message.Animation.FileID)
	} else if message.Photo != nil {
		txt = ""
		for i, p := range *message.Photo {
			txt += fmt.Sprintf("#%d: %s\n", i, p.FileID)
		}
	} else if message.Video != nil {
		txt = ""
		txt += fmt.Sprintf("Video ID: %s\nThumbnail Photo ID: %s", message.Video.FileID,
			message.Video.Thumbnail.FileID)
	}
	msg := telegram.NewMessage(message.Message.Chat.ID, txt)
	msg.ReplyToMessageID = message.Message.MessageID
	if _, err := bot.Send(msg); err != nil {
		log.Printf("Error sending: %v", err)
	}
	return nil
}

var MediaIDTrigger = &events.EventAction{
	Name:       "MediaIdTrigger",
	MatchFuncs: []events.MatchFunc{mediaIDTriggerMatch},
	ActionFunc: mediaIDTriggerAction,
}
