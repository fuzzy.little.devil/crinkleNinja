package model

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
)

func dataTableName() string {
	return fmt.Sprintf("%s-data", database.GetTablePrefix())
}

// Chat represents a Telegram Chat
type Data struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`
	Text    string `json:"text"`
	deleted bool
}

func (d Data) IsDeleted() bool {
	return d.deleted
}

func (d Data) ModelName() string {
	return "Data"
}

func (d Data) TableName() string {
	return dataTableName()
}

func (d Data) PrimaryKeyName() string {
	return "ID"
}

func (d Data) SortKeyNames() []string {
	return []string{}
}

func (d Data) EntityName() string {
	return d.Name
}

func (d Data) EntityID() int64 {
	return d.ID
}

func (d Data) PrimaryKey() *string {
	return aws.String(fmt.Sprintf("%d", d.ID))
}

func NewData(name, text string) *Data {
	c := &Data{ID: util.GenerateId(name), Name: name, Text: text}
	entities.Put(c)
	return c
}

func GetData(name string) *Data {
	e, err := entities.Get(&Data{Name: name, ID: util.GenerateId(name)})
	if err != nil {
		log.Printf("get error: %v\n", err)
		return nil
	}
	v := e.(*Data)
	return v
}

func (d *Data) Save() {
	if d.deleted {
		return
	}
	entities.Save(d, true)
	return
}

func (d *Data) Delete() {
	entities.Delete(d)
	d.deleted = true
	return
}
