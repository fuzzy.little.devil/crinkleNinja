package model

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"time"
)

func chatTableName() string {
	return fmt.Sprintf("%s-chat", database.GetTablePrefix())
}

var CurrentChat *Chat

// Chat represents a Telegram Chat
type Chat struct {
	tgbotapi.Chat
	Admins      map[int]string                 `json:"admins"`
	LastMessage time.Time                      `json:"last_message"`
	Events      map[string]*entities.EventData `json:"events"`
	deleted     bool
}

func (c Chat) IsDeleted() bool {
	return c.deleted
}

func (c Chat) ModelName() string {
	return "Chat"
}

func (c Chat) TableName() string {
	return chatTableName()
}

func (c Chat) PrimaryKeyName() string {
	return "ChatID"
}

func (c Chat) SortKeyNames() []string {
	return []string{}
}

func (c Chat) EntityName() string {
	if c.IsPrivate() {
		return fmt.Sprintf("PM: %s", c.UserName)
	}
	return c.Title
}

func (c Chat) EntityID() int64 {
	return c.ID
}

func (c Chat) PrimaryKey() *string {
	return aws.String(fmt.Sprintf("%d", c.ID))
}

func (c Chat) DiffersFrom(chat *tgbotapi.Chat) bool {
	if chat.LastName != c.LastName ||
		chat.FirstName != c.FirstName ||
		chat.UserName != c.UserName ||
		chat.Title != c.Title ||
		chat.AllMembersAreAdmins != c.AllMembersAreAdmins ||
		chat.Description != c.Description ||
		chat.InviteLink != c.InviteLink ||
		chat.Photo != c.Photo ||
		chat.Type != c.Type {
		return true
	}
	return false
}

func (c *Chat) UpdateFrom(chat *tgbotapi.Chat) {
	c.Chat = *chat
	c.LastName = chat.LastName
	c.FirstName = chat.FirstName
	c.UserName = chat.UserName
	c.Title = chat.Title
	c.AllMembersAreAdmins = chat.AllMembersAreAdmins
	c.Description = chat.Description
	c.InviteLink = chat.InviteLink
	c.Photo = chat.Photo
	c.Type = chat.Type
}

func AddChatFromMessage(message *messaging.Message) *Chat {
	if message.Message == nil || message.Message.Chat == nil {
		return nil
	}
	chat := getChat(message.Message.Chat)
	if chat == nil {
		chat = newChat(message.Message.Chat)
	}
	chat.LastMessage = time.Now()
	chat.Save()
	CurrentChat = chat
	return chat
}

// newChat creates and populates a new Chat
func newChat(chat *tgbotapi.Chat) *Chat {
	c := &Chat{Chat: *chat, Events: map[string]*entities.EventData{}}
	entities.Put(c)
	return c
}

// PopulateAdmins populates the Admins map with the list of admin members
func (c *Chat) PopulateAdmins(members []tgbotapi.ChatMember) {
	c.Admins = make(map[int]string)
	for _, member := range members {
		config.LogDebug("Adding (%d) %s to admin list for %s", member.User.ID, member.User.UserName, c.Title)
		c.Admins[member.User.ID] = member.User.UserName
	}
	c.Save()
}

// IsPrivate determines if the chat is private
func (c *Chat) IsPrivate() bool {
	return c.Type == "private"
}

// IsAdmin checks the internal Admins list to see if the provided user is an admin
func (c *Chat) IsAdmin(userID int) bool {
	_, ok := c.Admins[userID]
	return ok
}

func getChat(chat *tgbotapi.Chat) *Chat {
	e, err := entities.Get(&Chat{Chat: *chat})
	if err != nil {
		log.Printf("get error: %v\n", err)
		return nil
	}
	c := e.(*Chat)
	if c.DiffersFrom(chat) {
		c.UpdateFrom(chat)
		c.SaveNow()
	}
	return c
}

func (c *Chat) Save() {
	entities.Save(c, false)
	return
}

func (c *Chat) SaveNow() {
	entities.Save(c, true)
	return
}

func GetAllChats() []*Chat {
	input := &dynamodb.ScanInput{
		TableName: aws.String(chatTableName()),
	}
	var chats []*Chat
	result, err := database.Scan(input)
	if err != nil || result == nil || result.Items == nil {
		return chats
	}
	for _, item := range result.Items {
		if item["JSON"] == nil {
			continue
		}
		js := *item["JSON"].S
		if config.IsDebug() {
			fmt.Println(js)
		}
		c := &Chat{}
		err = json.Unmarshal([]byte(js), c)
		if err != nil {
			log.Printf("json: %v\n", err)
		}
		chats = append(chats, c)
	}
	return chats
}

func (c *Chat) UpdateChatAdmins(bot *telegram.Bot) {
	if !c.IsPrivate() {
		members, err := bot.GetChatAdministrators(c.ChatConfig())
		if err != nil {
			fmt.Printf("error getting chat administrators: %v", err)
		}
		c.PopulateAdmins(members)
	}
}
