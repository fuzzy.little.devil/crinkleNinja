package model

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
)

func eventActionTableName() string {
	return fmt.Sprintf("%s-event-action-data", database.GetTablePrefix())
}

// Chat represents a Telegram Chat
type EventActionData struct {
	ID              int64       `json:"ID"`
	EventActionName string      `json:"EventActionName"`
	Data            interface{} `json:"Data"`
	deleted         bool
}

func (ead EventActionData) IsDeleted() bool {
	return ead.deleted
}

func (ead EventActionData) ModelName() string {
	return "EventAction"
}

func (ead EventActionData) TableName() string {
	return eventActionTableName()
}

func (ead EventActionData) PrimaryKeyName() string {
	return "EventActionID"
}

func (ead EventActionData) SortKeyNames() []string {
	return []string{}
}

func (ead EventActionData) EntityName() string {
	return ead.EventActionName
}

func (ead EventActionData) EntityID() int64 {
	return ead.ID
}

func (ead EventActionData) PrimaryKey() *string {
	return aws.String(fmt.Sprintf("%d", ead.ID))
}

func GetOrCreateEventActionData(name string) *EventActionData {
	ead := GetEventActionData(name)
	if ead == nil {
		ead = NewEventActionData(name)
	}
	return ead
}

func NewEventActionData(name string) *EventActionData {
	ead := &EventActionData{
		EventActionName: name,
		ID:              util.GenerateId(name),
	}
	entities.Put(ead)
	return ead
}

func GetEventActionData(name string) *EventActionData {
	e, err := entities.Get(&EventActionData{EventActionName: name, ID: util.GenerateId(name)})
	if err != nil {
		log.Printf("get error: %v\n", err)
		return nil
	}
	ead := e.(*EventActionData)
	return ead
}

func (ead *EventActionData) Save() {
	if ead == nil {
		log.Println("tried to update nil event action data")
		return
	}
	entities.Save(ead, false)
	return
}
