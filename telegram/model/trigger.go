package model

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/keenan-v1/johnnycache/expiry"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
	"time"
)

var triggersCache = expiry.New()
var cacheExpiry = time.Hour

func triggerTableName() string {
	return fmt.Sprintf("%s-trigger", database.GetTablePrefix())
}

// Chat represents a Telegram Chat
type Trigger struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	Help        string `json:"help,omitempty"`
	MatchScript string `json:"match_script"`
	Script      string `json:"script"`
	deleted     bool
}

func (t Trigger) IsDeleted() bool {
	return t.deleted
}

func (t Trigger) ModelName() string {
	return "Trigger"
}

func (t Trigger) TableName() string {
	return triggerTableName()
}

func (t Trigger) PrimaryKeyName() string {
	return "ID"
}

func (t Trigger) SortKeyNames() []string {
	return []string{}
}

func (t Trigger) EntityName() string {
	return t.Name
}

func (t Trigger) EntityID() int64 {
	return t.ID
}

func (t Trigger) PrimaryKey() *string {
	return aws.String(fmt.Sprintf("%d", t.ID))
}

func NewTrigger(name, matchScript, script string) *Trigger {
	c := &Trigger{ID: util.GenerateId(name), Name: name, MatchScript: matchScript, Script: script}
	entities.Put(c)
	triggersCache.Delete(triggerTableName())
	return c
}

func GetTrigger(name string) *Trigger {
	e, err := entities.Get(&Trigger{Name: name, ID: util.GenerateId(name)})
	if err != nil {
		log.Printf("get error: %v\n", err)
		return nil
	}
	v := e.(*Trigger)
	return v
}

func (t *Trigger) Save() {
	entities.Save(t, false)
	triggersCache.Delete(triggerTableName())
	return
}

func (t *Trigger) Delete() {
	entities.Delete(t)
	triggersCache.Delete(triggerTableName())
	t.deleted = true
	return
}

func GetAllTriggers() []*Trigger {
	val, ok := triggersCache.Load(triggerTableName())
	if ok {
		return val.([]*Trigger)
	}
	input := &dynamodb.ScanInput{
		TableName: aws.String(triggerTableName()),
	}
	var triggers []*Trigger
	result, err := database.Scan(input)
	if err != nil || result == nil || result.Items == nil {
		return triggers
	}
	for _, item := range result.Items {
		if item["JSON"] == nil {
			continue
		}
		js := *item["JSON"].S
		if config.IsDebug() {
			fmt.Println(js)
		}
		c := &Trigger{}
		err = json.Unmarshal([]byte(js), c)
		if err != nil {
			log.Printf("json: %v\n", err)
		}
		triggers = append(triggers, c)
	}
	triggersCache.Store(triggerTableName(), triggers, cacheExpiry)
	return triggers
}
