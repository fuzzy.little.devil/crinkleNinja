package model

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"log"
)

func commandTableName() string {
	return fmt.Sprintf("%s-command", database.GetTablePrefix())
}

// Chat represents a Telegram Chat
type Command struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`
	Help    string `json:"help,omitempty"`
	Script  string `json:"script"`
	deleted bool
}

func (c Command) IsDeleted() bool {
	return c.deleted
}

func (c Command) ModelName() string {
	return "Command"
}

func (c Command) TableName() string {
	return commandTableName()
}

func (c Command) PrimaryKeyName() string {
	return "ID"
}

func (c Command) SortKeyNames() []string {
	return []string{}
}

func (c Command) EntityName() string {
	return c.Name
}

func (c Command) EntityID() int64 {
	return c.ID
}

func (c Command) PrimaryKey() *string {
	return aws.String(fmt.Sprintf("%d", c.ID))
}

func NewCommand(name, script string) *Command {
	c := &Command{ID: util.GenerateId(name), Name: name, Script: script}
	entities.Put(c)
	return c
}

func GetCommand(name string) *Command {
	e, err := entities.Get(&Command{Name: name, ID: util.GenerateId(name)})
	if err != nil {
		log.Printf("get error: %v\n", err)
		return nil
	}
	v := e.(*Command)
	return v
}

func (c *Command) Save() {
	entities.Save(c, false)
	return
}

func (c *Command) Delete() {
	entities.Delete(c)
	c.deleted = true
	return
}

func GetAllCommands() []*Command {
	input := &dynamodb.ScanInput{
		TableName: aws.String(commandTableName()),
	}
	var commands []*Command
	result, err := database.Scan(input)
	if err != nil || result == nil || result.Items == nil {
		return commands
	}
	for _, item := range result.Items {
		if item["JSON"] == nil {
			continue
		}
		js := *item["JSON"].S
		if config.IsDebug() {
			fmt.Println(js)
		}
		c := &Command{}
		err = json.Unmarshal([]byte(js), c)
		if err != nil {
			log.Printf("json: %v\n", err)
		}
		commands = append(commands, c)
	}
	return commands
}
