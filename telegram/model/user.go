package model

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"time"
)

func userTableName() string {
	return fmt.Sprintf("%s-user", database.GetTablePrefix())
}

var CurrentUser *User

// User represents a Telegram user
// For now, we're a decorator
type User struct {
	tgbotapi.User
	LastSeen         time.Time                      `json:"last_seen"`
	Events           map[string]*entities.EventData `json:"events"`
	CurrentChatAdmin bool                           `json:"-"`
	deleted          bool
}

func (u User) IsDeleted() bool {
	return u.deleted
}

func (u User) ModelName() string {
	return "User"
}

func (u User) TableName() string {
	return userTableName()
}

func (u User) PrimaryKeyName() string {
	return "UserID"
}

func (u User) SortKeyNames() []string {
	return []string{}
}

func (u User) EntityName() string {
	return u.UserName
}

func (u User) EntityID() int64 {
	return int64(u.ID)
}

func (u User) PrimaryKey() *string {
	return aws.String(fmt.Sprintf("%d", u.ID))
}

func (u User) DiffersFrom(user *tgbotapi.User) bool {
	if u.UserName != user.UserName ||
		u.FirstName != user.FirstName ||
		u.LastName != user.LastName {
		return true
	}
	return false
}

func (u *User) UpdateFrom(user *tgbotapi.User) {
	u.User = *user
	u.UserName = user.UserName
	u.FirstName = user.FirstName
	u.LastName = user.LastName
	u.IsBot = user.IsBot
}

func (u User) GetActiveContextName() string {
	if u.Events != nil {
		for _, event := range u.Events {
			if event.ActiveContext != nil {
				return *event.ActiveContext
			}
		}
	}
	return ""
}

// AddChat adds a chat to the map
func AddUserFromMessage(message *messaging.Message) *User {
	if message.Message == nil || message.Message.From == nil {
		return nil
	}
	user := getUser(message.Message.From)
	if user == nil {
		user = newUser(message.Message.From)
	}
	user.LastSeen = time.Now()
	user.Save()
	CurrentUser = user
	return user
}

// newUser creates a User and populates the internal struct tgbotapi.User
func newUser(user *tgbotapi.User) *User {
	u := &User{User: *user, Events: map[string]*entities.EventData{}}
	entities.Put(u)
	return u
}

func getUser(user *tgbotapi.User) *User {
	e, err := entities.Get(&User{User: *user})
	if err != nil {
		log.Printf("get error: %v\n", err)
		return nil
	}
	u := e.(*User)
	if u.DiffersFrom(user) {
		u.UpdateFrom(user)
		u.SaveNow()
	}
	return u
}

func (u *User) SaveNow() {
	entities.Save(u, true)
}

func (u *User) Save() {
	entities.Save(u, false)
}
