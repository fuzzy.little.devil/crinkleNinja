package lua_api

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestLua(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Lua Suite")
}
