package lua_api_test

import (
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/lua_api"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var chat = tgbotapi.Chat{
	ID:                  123456789,
	Type:                "supergroup",
	Title:               "Test Chat",
	UserName:            "testchat",
	FirstName:           "test",
	LastName:            "chat",
	AllMembersAreAdmins: false,
	Photo:               nil,
	Description:         "test chat",
	InviteLink:          "http://example.com",
}

var user = tgbotapi.User{
	ID:           1234,
	FirstName:    "Test",
	LastName:     "McTesty",
	UserName:     "testMcTesty",
	LanguageCode: "",
	IsBot:        false,
}

var animation = tgbotapi.ChatAnimation{
	FileID:    "testID",
	Width:     12,
	Height:    34,
	Duration:  123,
	Thumbnail: nil,
	FileName:  "TestName",
	MimeType:  "testType",
	FileSize:  1234,
}

var audio = tgbotapi.Audio{
	FileID:    "testID",
	Duration:  12,
	Performer: "Test",
	Title:     "McTest",
	MimeType:  "testType",
	FileSize:  1234,
}

var document = tgbotapi.Document{
	FileID:    "testID",
	Thumbnail: nil,
	FileName:  "testName",
	MimeType:  "testType",
	FileSize:  1234,
}

var testMessage = &messaging.Message{
	Message: &tgbotapi.Message{
		MessageID: 123456789,
		Animation: &animation,
		Audio:     &audio,
		Document:  &document,
		From:      &user,
		Chat:      &chat,
		Text:      "test",
	},
}
var _ = Describe("Lua", func() {
	Describe("Passing Script", func() {
		var scriptFiles []string
		err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
			if filepath.Ext(path) == ".lua" && strings.Contains(path, "passing") {
				scriptFiles = append(scriptFiles, path)
			}
			return nil
		})
		if err != nil {
			panic(err)
		}
		for _, file := range scriptFiles {
			b, err := ioutil.ReadFile(file)
			if err != nil {
				panic(err)
			}
			ea := &events.EventAction{}
			message := testMessage
			bot := &telegram.MockBot{}
			luaState := lua.NewState(lua.Options{
				IncludeGoStackTrace: true,
			})
			ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
			luaConfig := &embedded.LuaConfig{
				Script: string(b),
				Globals: map[string]embedded.LuaValueFunc{
					"event":   lua_api.AsLuaType(ea, lua_api.EventActionLuaTypeName),
					"message": lua_api.AsLuaType(message, lua_api.MessageLuaTypeName),
					"bot":     lua_api.AsLuaType(bot, lua_api.BotLuaTypeName),
					"chat":    lua_api.AsLuaType(model.CurrentChat, lua_api.ChatLuaTypeName),
					"user":    lua_api.AsLuaType(model.CurrentUser, lua_api.UserLuaTypeName),
				},
				State:      luaState,
				Types:      lua_api.TypeRegistry,
				Modules:    lua_api.ModuleRegistry,
				Context:    ctx,
				CancelFunc: cancel,
			}
			_, err = embedded.RunLua(luaConfig)
			Context("With script "+file, func() {
				It("should not error", func() {
					Expect(err).Should(BeNil())
				})
			})
		}
	})
	Describe("Failing Script", func() {
		var scriptFiles []string
		err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
			if filepath.Ext(path) == ".lua" && strings.Contains(path, "failing") {
				scriptFiles = append(scriptFiles, path)
			}
			return nil
		})
		if err != nil {
			panic(err)
		}
		for _, file := range scriptFiles {
			b, err := ioutil.ReadFile(file)
			if err != nil {
				panic(err)
			}
			ea := &events.EventAction{}
			message := testMessage
			bot := &telegram.MockBot{}
			luaState := lua.NewState(lua.Options{
				IncludeGoStackTrace: true,
			})
			ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
			luaConfig := &embedded.LuaConfig{
				Script: string(b),
				Globals: map[string]embedded.LuaValueFunc{
					"event":   lua_api.AsLuaType(ea, lua_api.EventActionLuaTypeName),
					"message": lua_api.AsLuaType(message, lua_api.MessageLuaTypeName),
					"bot":     lua_api.AsLuaType(bot, lua_api.BotLuaTypeName),
					"chat":    lua_api.AsLuaType(model.CurrentChat, lua_api.ChatLuaTypeName),
					"user":    lua_api.AsLuaType(model.CurrentUser, lua_api.UserLuaTypeName),
				},
				State:      luaState,
				Types:      lua_api.TypeRegistry,
				Modules:    lua_api.ModuleRegistry,
				Context:    ctx,
				CancelFunc: cancel,
			}
			_, err = embedded.RunLua(luaConfig)
			Context("With script "+file, func() {
				It("should error", func() {
					Expect(err).ShouldNot(BeNil())
				})
			})
		}
	})
})
