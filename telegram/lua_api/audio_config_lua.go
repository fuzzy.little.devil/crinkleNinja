package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const AudioConfigLuaTypeName = "audio_config"

var audioConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fileId":              luaBaseFileFileID,
	"useExisting":         luaBaseFileUseExisting,
	"caption":             luaAudioConfigCaption,
	"parseMode":           luaAudioConfigParseMode,
	"duration":            luaAudioConfigDuration,
	"performer":           luaAudioConfigPerformer,
	"title":               luaAudioConfigTitle,
}

func RegisterAudioConfigType() (string, embedded.LuaTypeFunc) {
	return AudioConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(AudioConfigLuaTypeName)
		L.SetGlobal(AudioConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newAudioConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), audioConfigLuaMethods))
	}
}

func newAudioConfig(L *lua.LState) int {
	cfg := telegram.AudioConfig{AudioConfig: tgbotapi.AudioConfig{BaseFile: tgbotapi.BaseFile{UseExisting: true}}}
	if L.GetTop() == 2 {
		cfg = telegram.NewAudioShare(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, AudioConfigLuaTypeName)(L))
	return 1
}

func checkAudioConfig(L *lua.LState) *telegram.AudioConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.AudioConfig)
	if !ok {
		L.ArgError(1, AudioConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaAudioConfigCaption(L *lua.LState) int {
	v := checkAudioConfig(L)
	if L.GetTop() == 2 {
		v.Caption = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Caption))
	return 1
}

func luaAudioConfigParseMode(L *lua.LState) int {
	v := checkAudioConfig(L)
	if L.GetTop() == 2 {
		v.ParseMode = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.ParseMode))
	return 1
}

func luaAudioConfigDuration(L *lua.LState) int {
	v := checkAudioConfig(L)
	if L.GetTop() == 2 {
		v.Duration = int(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(v.Duration))
	return 1
}

func luaAudioConfigPerformer(L *lua.LState) int {
	v := checkAudioConfig(L)
	if L.GetTop() == 2 {
		v.Performer = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Performer))
	return 1
}

func luaAudioConfigTitle(L *lua.LState) int {
	v := checkAudioConfig(L)
	if L.GetTop() == 2 {
		v.Title = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Title))
	return 1
}
