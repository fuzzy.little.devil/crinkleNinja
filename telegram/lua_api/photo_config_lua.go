package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const PhotoConfigLuaTypeName = "photo_config"

var photoConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fileId":              luaBaseFileFileID,
	"useExisting":         luaBaseFileUseExisting,
	"caption":             luaPhotoConfigCaption,
	"parseMode":           luaPhotoConfigParseMode,
}

func RegisterPhotoConfigType() (string, embedded.LuaTypeFunc) {
	return PhotoConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(PhotoConfigLuaTypeName)
		L.SetGlobal(PhotoConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newPhotoConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), photoConfigLuaMethods))
	}
}

func newPhotoConfig(L *lua.LState) int {
	cfg := telegram.PhotoConfig{PhotoConfig: tgbotapi.PhotoConfig{BaseFile: tgbotapi.BaseFile{UseExisting: true}}}
	if L.GetTop() == 2 {
		cfg = telegram.NewPhotoShare(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, PhotoConfigLuaTypeName)(L))
	return 1
}

func checkPhotoConfig(L *lua.LState) *telegram.PhotoConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.PhotoConfig)
	if !ok {
		L.ArgError(1, PhotoConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaPhotoConfigCaption(L *lua.LState) int {
	v := checkPhotoConfig(L)
	if L.GetTop() == 2 {
		v.Caption = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Caption))
	return 1
}

func luaPhotoConfigParseMode(L *lua.LState) int {
	v := checkPhotoConfig(L)
	if L.GetTop() == 2 {
		v.ParseMode = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.ParseMode))
	return 1
}
