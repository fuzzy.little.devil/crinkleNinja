package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

const DataModuleName = "bot.data"

var dataModuleExports = map[string]lua.LGFunction{
	"get":    dataGetKey,
	"new":    dataNewKey,
	"delete": dataDeleteKey,
	"exists": dataExistsKey,
}

func DataModuleLoader(L *lua.LState) int {
	mod := L.SetFuncs(L.NewTable(), dataModuleExports)
	L.SetField(mod, "name", lua.LString(DataModuleName))
	L.Push(mod)
	return 1
}

func DataModuleRegister() (string, lua.LGFunction) {
	return DataModuleName, DataModuleLoader
}

func dataGetKey(L *lua.LState) int {
	key := L.CheckString(1)
	d := model.GetData(key)
	if d == nil {
		L.Push(lua.LNil)
	} else {
		ud := L.NewUserData()
		ud.Value = d
		L.SetMetatable(ud, L.GetTypeMetatable(DataLuaTypeName))
		L.Push(ud)
	}
	return 1
}

func dataExistsKey(L *lua.LState) int {
	key := L.CheckString(1)
	d := model.GetData(key)
	L.Push(lua.LBool(d != nil))
	return 1
}

func dataNewKey(L *lua.LState) int {
	key := L.CheckString(1)
	if model.GetData(key) != nil {
		L.ArgError(1, "key already exists")
	}
	d := model.NewData(key, "")
	ud := L.NewUserData()
	ud.Value = d
	L.SetMetatable(ud, L.GetTypeMetatable(DataLuaTypeName))
	L.Push(ud)
	return 1
}

func dataDeleteKey(L *lua.LState) int {
	key := L.CheckString(1)
	d := model.GetData(key)
	d.Delete()
	return 0
}

var DataLuaTypeName = "bot_data"

var dataLuaMethods = map[string]lua.LGFunction{
	"key":    luaGetKey,
	"text":   luaGetSetText,
	"save":   luaSaveData,
	"delete": luaDeleteData,
}

func RegisterDataType() (string, embedded.LuaTypeFunc) {
	return DataLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(DataLuaTypeName)
		L.SetGlobal(DataLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), dataLuaMethods))
	}
}

func checkData(L *lua.LState) *model.Data {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*model.Data); ok {
		return v
	}
	L.ArgError(1, DataLuaTypeName+" expected")
	return nil
}

func luaGetKey(L *lua.LState) int {
	d := checkData(L)
	L.Push(lua.LString(d.Name))
	return 1
}

func luaGetSetText(L *lua.LState) int {
	d := checkData(L)
	if L.GetTop() == 2 {
		d.Text = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(d.Text))
	return 1
}

func luaSaveData(L *lua.LState) int {
	d := checkData(L)
	d.Save()
	return 0
}

func luaDeleteData(L *lua.LState) int {
	d := checkData(L)
	d.Delete()
	return 0
}
