package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const DocumentConfigLuaTypeName = "document_config"

var documentConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fileId":              luaBaseFileFileID,
	"useExisting":         luaBaseFileUseExisting,
	"caption":             luaDocumentConfigCaption,
	"parseMode":           luaDocumentConfigParseMode,
}

func RegisterDocumentConfigType() (string, embedded.LuaTypeFunc) {
	return DocumentConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(DocumentConfigLuaTypeName)
		L.SetGlobal(DocumentConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newDocumentConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), documentConfigLuaMethods))
	}
}

func newDocumentConfig(L *lua.LState) int {
	cfg := telegram.DocumentConfig{DocumentConfig: tgbotapi.DocumentConfig{BaseFile: tgbotapi.BaseFile{UseExisting: true}}}
	if L.GetTop() == 2 {
		cfg = telegram.NewDocumentShare(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, DocumentConfigLuaTypeName)(L))
	return 1
}

func checkDocumentConfig(L *lua.LState) *telegram.DocumentConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.DocumentConfig)
	if !ok {
		L.ArgError(1, DocumentConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaDocumentConfigCaption(L *lua.LState) int {
	v := checkDocumentConfig(L)
	if L.GetTop() == 2 {
		v.Caption = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Caption))
	return 1
}

func luaDocumentConfigParseMode(L *lua.LState) int {
	v := checkDocumentConfig(L)
	if L.GetTop() == 2 {
		v.ParseMode = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.ParseMode))
	return 1
}
