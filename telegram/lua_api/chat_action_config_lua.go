package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
)

const ChatActionConfigLuaTypeName = "chat_action_config"

var chatActionConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"action":              luaChatActionConfigAction,
}

func RegisterChatActionConfigType() (string, embedded.LuaTypeFunc) {
	return ChatActionConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(ChatActionConfigLuaTypeName)
		L.SetGlobal(ChatActionConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newChatActionConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), chatActionConfigLuaMethods))
	}
}

func newChatActionConfig(L *lua.LState) int {
	cfg := telegram.ChatActionConfig{}
	if L.GetTop() == 2 {
		cfg = telegram.NewChatAction(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, ChatActionConfigLuaTypeName)(L))
	return 1
}

func checkChatActionConfig(L *lua.LState) *telegram.ChatActionConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.ChatActionConfig)
	if !ok {
		L.ArgError(1, ChatActionConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaChatActionConfigAction(L *lua.LState) int {
	ca := checkChatActionConfig(L)
	if L.GetTop() == 2 {
		ca.Action = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(ca.Action))
	return 1
}
