package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const AnimationConfigLuaTypeName = "animation_config"

var animationConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fileId":              luaBaseFileFileID,
	"useExisting":         luaBaseFileUseExisting,
	"caption":             luaAnimationConfigCaption,
	"parseMode":           luaAnimationConfigParseMode,
	"duration":            luaAnimationConfigDuration,
}

func RegisterAnimationConfigType() (string, embedded.LuaTypeFunc) {
	return AnimationConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(AnimationConfigLuaTypeName)
		L.SetGlobal(AnimationConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newAnimationConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), animationConfigLuaMethods))
	}
}

func newAnimationConfig(L *lua.LState) int {
	cfg := telegram.AnimationConfig{AnimationConfig: tgbotapi.AnimationConfig{BaseFile: tgbotapi.BaseFile{UseExisting: true}}}
	if L.GetTop() == 2 {
		cfg = telegram.NewAnimationShare(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, AnimationConfigLuaTypeName)(L))
	return 1
}

func checkAnimationConfig(L *lua.LState) *telegram.AnimationConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.AnimationConfig)
	if !ok {
		L.ArgError(1, AnimationConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaAnimationConfigCaption(L *lua.LState) int {
	v := checkAnimationConfig(L)
	if L.GetTop() == 2 {
		v.Caption = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Caption))
	return 1
}

func luaAnimationConfigParseMode(L *lua.LState) int {
	v := checkAnimationConfig(L)
	if L.GetTop() == 2 {
		v.ParseMode = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.ParseMode))
	return 1
}

func luaAnimationConfigDuration(L *lua.LState) int {
	v := checkAnimationConfig(L)
	if L.GetTop() == 2 {
		v.Duration = int(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(v.Duration))
	return 1
}
