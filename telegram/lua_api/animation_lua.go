package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const AnimationLuaTypeName = "telegram_animation"

var animationLuaMethods = map[string]lua.LGFunction{
	"id":        luaAnimationGetFileID,
	"fileSize":  luaAnimationGetFileSize,
	"duration":  luaAnimationGetDuration,
	"mimeType":  luaAnimationGetMimeType,
	"width":     luaAnimationGetWidth,
	"height":    luaAnimationGetHeight,
	"thumbnail": luaAnimationGetThumbnail,
	"fileName":  luaAnimationGetFileName,
}

func RegisterAnimationType() (string, embedded.LuaTypeFunc) {
	return AnimationLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(AnimationLuaTypeName)
		L.SetGlobal(AnimationLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), animationLuaMethods))
	}
}

func checkAnimation(L *lua.LState) *tgbotapi.ChatAnimation {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tgbotapi.ChatAnimation); ok {
		return v
	}
	L.ArgError(1, AnimationLuaTypeName+" expected")
	return nil
}

func luaAnimationGetFileID(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LString(animation.FileID))
	return 1
}

func luaAnimationGetFileSize(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LNumber(animation.FileSize))
	return 1
}

func luaAnimationGetDuration(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LNumber(animation.Duration))
	return 1
}

func luaAnimationGetMimeType(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LString(animation.MimeType))
	return 1
}

func luaAnimationGetWidth(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LNumber(animation.Width))
	return 1
}

func luaAnimationGetHeight(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LNumber(animation.Height))
	return 1
}

func luaAnimationGetThumbnail(L *lua.LState) int {
	animation := checkAnimation(L)
	if animation.Thumbnail == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(animation.Thumbnail, PhotoLuaTypeName)(L))
	}
	return 1
}

func luaAnimationGetFileName(L *lua.LState) int {
	animation := checkAnimation(L)
	L.Push(lua.LString(animation.FileName))
	return 1
}
