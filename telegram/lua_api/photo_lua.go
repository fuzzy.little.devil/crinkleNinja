package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const PhotoLuaTypeName = "telegram_photo"

var photoLuaMethods = map[string]lua.LGFunction{
	"id":       luaPhotoGetFileID,
	"fileSize": luaPhotoGetFileSize,
	"height":   luaPhotoGetHeight,
	"width":    luaPhotoGetWidth,
}

func RegisterPhotoType() (string, embedded.LuaTypeFunc) {
	return PhotoLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(PhotoLuaTypeName)
		L.SetGlobal(PhotoLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), photoLuaMethods))
	}
}

func checkPhoto(L *lua.LState) *tgbotapi.PhotoSize {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tgbotapi.PhotoSize); ok {
		return v
	}
	L.ArgError(1, PhotoLuaTypeName+" expected")
	return nil
}

func luaPhotoGetFileID(L *lua.LState) int {
	photo := checkPhoto(L)
	L.Push(lua.LString(photo.FileID))
	return 1
}

func luaPhotoGetFileSize(L *lua.LState) int {
	photo := checkPhoto(L)
	L.Push(lua.LNumber(photo.FileSize))
	return 1
}

func luaPhotoGetHeight(L *lua.LState) int {
	photo := checkPhoto(L)
	L.Push(lua.LNumber(photo.Height))
	return 1
}

func luaPhotoGetWidth(L *lua.LState) int {
	photo := checkPhoto(L)
	L.Push(lua.LNumber(photo.Width))
	return 1
}
