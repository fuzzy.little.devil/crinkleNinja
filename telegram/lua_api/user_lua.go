package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

const UserLuaTypeName = "telegram_user"

var userLuaMethods = map[string]lua.LGFunction{
	"id":        luaGetUserId,
	"username":  luaGetUserName,
	"firstName": luaGetUserFirstName,
	"lastName":  luaGetUserLastName,
}

func RegisterUserType() (string, embedded.LuaTypeFunc) {
	return UserLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(UserLuaTypeName)
		L.SetGlobal(UserLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), userLuaMethods))
	}
}

func checkUser(L *lua.LState) *model.User {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*model.User); ok {
		return v
	}
	L.ArgError(1, UserLuaTypeName+" expected")
	return nil
}

func luaGetUserId(L *lua.LState) int {
	v := checkUser(L)
	L.Push(lua.LNumber(v.ID))
	return 1
}

func luaGetUserName(L *lua.LState) int {
	v := checkUser(L)
	L.Push(lua.LString(v.UserName))
	return 1
}

func luaGetUserFirstName(L *lua.LState) int {
	v := checkUser(L)
	L.Push(lua.LString(v.FirstName))
	return 1
}

func luaGetUserLastName(L *lua.LState) int {
	v := checkUser(L)
	L.Push(lua.LString(v.LastName))
	return 1
}
