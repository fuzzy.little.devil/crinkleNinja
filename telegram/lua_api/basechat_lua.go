package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"reflect"
)

func checkBaseChat(L *lua.LState) *tgbotapi.BaseChat {
	value := reflect.ValueOf(L.CheckUserData(1).Value).Elem().FieldByName("BaseChat")
	if ret, ok := value.Addr().Interface().(*tgbotapi.BaseChat); ok {
		return ret
	}
	L.ArgError(1, "expected a base chat object")
	return nil
}

func luaBaseChatChatID(L *lua.LState) int {
	c := checkBaseChat(L)
	if L.GetTop() == 2 {
		c.ChatID = int64(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(c.ChatID))
	return 1
}

func luaBaseChatChannelUsername(L *lua.LState) int {
	c := checkBaseChat(L)
	if L.GetTop() == 2 {
		c.ChannelUsername = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(c.ChannelUsername))
	return 1
}

func luaBaseChatReplyToMessageID(L *lua.LState) int {
	c := checkBaseChat(L)
	if L.GetTop() == 2 {
		c.ReplyToMessageID = int(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(c.ReplyToMessageID))
	return 1
}

func luaBaseChatDisableNotification(L *lua.LState) int {
	c := checkBaseChat(L)
	if L.GetTop() == 2 {
		c.DisableNotification = L.CheckBool(2)
		return 0
	}
	L.Push(lua.LBool(c.DisableNotification))
	return 1
}
