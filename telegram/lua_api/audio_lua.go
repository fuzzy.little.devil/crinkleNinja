package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const AudioLuaTypeName = "telegram_audio"

var audioLuaMethods = map[string]lua.LGFunction{
	"id":        luaAudioGetFileID,
	"fileSize":  luaAudioGetFileSize,
	"duration":  luaAudioGetDuration,
	"performer": luaAudioGetPerformer,
	"title":     luaAudioGetTitle,
	"mimeType":  luaAudioGetMimeType,
}

func RegisterAudioType() (string, embedded.LuaTypeFunc) {
	return AudioLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(AudioLuaTypeName)
		L.SetGlobal(AudioLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), audioLuaMethods))
	}
}

func checkAudio(L *lua.LState) *tgbotapi.Audio {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tgbotapi.Audio); ok {
		return v
	}
	L.ArgError(1, AudioLuaTypeName+" expected")
	return nil
}

func luaAudioGetFileID(L *lua.LState) int {
	audio := checkAudio(L)
	L.Push(lua.LString(audio.FileID))
	return 1
}

func luaAudioGetFileSize(L *lua.LState) int {
	audio := checkAudio(L)
	L.Push(lua.LNumber(audio.FileSize))
	return 1
}

func luaAudioGetDuration(L *lua.LState) int {
	audio := checkAudio(L)
	L.Push(lua.LNumber(audio.Duration))
	return 1
}

func luaAudioGetPerformer(L *lua.LState) int {
	audio := checkAudio(L)
	L.Push(lua.LString(audio.Performer))
	return 1
}

func luaAudioGetTitle(L *lua.LState) int {
	audio := checkAudio(L)
	L.Push(lua.LString(audio.Title))
	return 1
}

func luaAudioGetMimeType(L *lua.LState) int {
	audio := checkAudio(L)
	L.Push(lua.LString(audio.MimeType))
	return 1
}
