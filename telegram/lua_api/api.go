package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
)

func AsLuaType(v interface{}, typeName string) embedded.LuaValueFunc {
	return func(L *lua.LState) lua.LValue {
		ud := L.NewUserData()
		ud.Value = v
		L.SetMetatable(ud, L.GetTypeMetatable(typeName))
		return ud
	}
}

var TypeRegistry = []embedded.TypeRegisterFunc{
	// Global Types
	RegisterBotType,
	RegisterMessageType,
	RegisterEventActionType,
	// Message element types
	RegisterChatType,
	RegisterUserType,
	RegisterDataType,
	RegisterStickerType,
	RegisterPhotoType,
	RegisterAudioType,
	RegisterDocumentType,
	RegisterVideoType,
	RegisterAnimationType,
	// Chattable Config Types
	RegisterStickerConfigType,
	RegisterMessageConfigType,
	RegisterForwardConfigType,
	RegisterPhotoConfigType,
	RegisterChatActionConfigType,
	RegisterAudioConfigType,
	RegisterDocumentConfigType,
	RegisterVideoConfigType,
	RegisterAnimationConfigType,
}

var ModuleRegistry = []embedded.ModuleRegisterFunc{
	DataModuleRegister,
}
