package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const DocumentLuaTypeName = "telegram_document"

var documentLuaMethods = map[string]lua.LGFunction{
	"id":        luaDocumentGetFileID,
	"fileSize":  luaDocumentGetFileSize,
	"fileName":  luaDocumentGetFileName,
	"mimeType":  luaDocumentGetMimeType,
	"thumbnail": luaDocumentGetThumbnail,
}

func RegisterDocumentType() (string, embedded.LuaTypeFunc) {
	return DocumentLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(DocumentLuaTypeName)
		L.SetGlobal(DocumentLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), documentLuaMethods))
	}
}

func checkDocument(L *lua.LState) *tgbotapi.Document {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tgbotapi.Document); ok {
		return v
	}
	L.ArgError(1, DocumentLuaTypeName+" expected")
	return nil
}

func luaDocumentGetFileID(L *lua.LState) int {
	document := checkDocument(L)
	L.Push(lua.LString(document.FileID))
	return 1
}

func luaDocumentGetFileSize(L *lua.LState) int {
	document := checkDocument(L)
	L.Push(lua.LNumber(document.FileSize))
	return 1
}

func luaDocumentGetFileName(L *lua.LState) int {
	document := checkDocument(L)
	L.Push(lua.LString(document.FileName))
	return 1
}

func luaDocumentGetMimeType(L *lua.LState) int {
	document := checkDocument(L)
	L.Push(lua.LString(document.MimeType))
	return 1
}

func luaDocumentGetThumbnail(L *lua.LState) int {
	document := checkDocument(L)
	if document.Thumbnail == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(document.Thumbnail, PhotoLuaTypeName)(L))
	}
	return 1
}
