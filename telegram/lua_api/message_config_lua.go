package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
)

const MessageConfigLuaTypeName = "message_config"

var messageConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":                luaBaseChatChatID,
	"channelUsername":       luaBaseChatChannelUsername,
	"replyToMessageId":      luaBaseChatReplyToMessageID,
	"disableNotification":   luaBaseChatDisableNotification,
	"text":                  luaMessageConfigText,
	"parseMode":             luaMessageConfigParseMode,
	"disableWebPagePreview": luaMessageConfigDisableWebPagePreview,
}

func RegisterMessageConfigType() (string, embedded.LuaTypeFunc) {
	return MessageConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(MessageConfigLuaTypeName)
		L.SetGlobal(MessageConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newMessageConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), messageConfigLuaMethods))
	}
}

func newMessageConfig(L *lua.LState) int {
	cfg := telegram.MessageConfig{}
	if L.GetTop() == 2 {
		cfg = telegram.NewMessage(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, MessageConfigLuaTypeName)(L))
	return 1
}

func checkMessageConfig(L *lua.LState) *telegram.MessageConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.MessageConfig)
	if !ok {
		L.ArgError(1, MessageConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaMessageConfigText(L *lua.LState) int {
	mc := checkMessageConfig(L)
	if L.GetTop() == 2 {
		mc.Text = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(mc.Text))
	return 1
}

func luaMessageConfigParseMode(L *lua.LState) int {
	mc := checkMessageConfig(L)
	if L.GetTop() == 2 {
		mc.ParseMode = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(mc.ParseMode))
	return 1
}

func luaMessageConfigDisableWebPagePreview(L *lua.LState) int {
	mc := checkMessageConfig(L)
	if L.GetTop() == 2 {
		mc.DisableWebPagePreview = L.CheckBool(2)
		return 0
	}
	L.Push(lua.LBool(mc.DisableWebPagePreview))
	return 1
}
