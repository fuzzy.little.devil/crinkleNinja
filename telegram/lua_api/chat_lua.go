package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

const ChatLuaTypeName = "telegram_chat"

var chatLuaMethods = map[string]lua.LGFunction{
	"id":                  luaGetChatId,
	"title":               luaGetChatTitle,
	"username":            luaGetChatUserName,
	"firstName":           luaGetChatFirstName,
	"lastName":            luaGetChatLastName,
	"allMembersAreAdmins": luaGetChatAllMembersAreAdmins,
	"description":         luaGetChatDescription,
	"inviteLink":          luaGetChatInviteLink,
	"smallPhotoId":        luaGetChatSmallPhotoID,
	"bigPhotoId":          luaGetChatBigPhotoID,
	"isPrivate":           luaIsPrivateChat,
	"isGroup":             luaIsGroupChat,
	"isSuperGroup":        luaIsSuperGroupChat,
	"isChannel":           luaIsChannelChat,
	"admins":              luaGetChatAdmins,
}

func RegisterChatType() (string, embedded.LuaTypeFunc) {
	return ChatLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(ChatLuaTypeName)
		L.SetGlobal(ChatLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), chatLuaMethods))
	}
}

func checkChat(L *lua.LState) *model.Chat {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*model.Chat); ok {
		return v
	}
	L.ArgError(1, ChatLuaTypeName+" expected")
	return nil
}

func luaGetChatId(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LNumber(chat.Chat.ID))
	return 1
}

func luaGetChatTitle(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LString(chat.Chat.Title))
	return 1
}

func luaGetChatUserName(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LString(chat.Chat.UserName))
	return 1
}

func luaGetChatFirstName(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LString(chat.Chat.FirstName))
	return 1
}

func luaGetChatLastName(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LString(chat.Chat.LastName))
	return 1
}

func luaGetChatAllMembersAreAdmins(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LBool(chat.Chat.AllMembersAreAdmins))
	return 1
}

func luaGetChatDescription(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LString(chat.Chat.Description))
	return 1
}

func luaGetChatInviteLink(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LString(chat.Chat.InviteLink))
	return 1
}

func luaIsPrivateChat(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LBool(chat.IsPrivate()))
	return 1
}

func luaIsGroupChat(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LBool(chat.IsGroup()))
	return 1
}

func luaIsSuperGroupChat(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LBool(chat.IsSuperGroup()))
	return 1
}

func luaIsChannelChat(L *lua.LState) int {
	chat := checkChat(L)
	L.Push(lua.LBool(chat.IsChannel()))
	return 1
}

func luaGetChatSmallPhotoID(L *lua.LState) int {
	chat := checkChat(L)
	if chat.Photo == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LString(chat.Photo.SmallFileID))
	return 1
}

func luaGetChatBigPhotoID(L *lua.LState) int {
	chat := checkChat(L)
	if chat.Photo == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LString(chat.Photo.BigFileID))
	return 1
}

func luaGetChatAdmins(L *lua.LState) int {
	chat := checkChat(L)
	tbl := L.NewTable()
	for k, v := range chat.Admins {
		tbl.RawSetInt(k, lua.LString(v))
	}
	L.Push(tbl)
	return 1
}
