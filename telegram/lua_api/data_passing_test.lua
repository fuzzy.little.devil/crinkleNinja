local data = require("bot.data")

local d = data.new("test")
d:text("testing")
assert(d:text() == "testing")
assert(d:key() == "test")
d:save()
d:delete()