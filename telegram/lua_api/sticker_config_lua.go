package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const StickerConfigLuaTypeName = "sticker_config"

var stickerConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fileId":              luaBaseFileFileID,
	"useExisting":         luaBaseFileUseExisting,
}

func RegisterStickerConfigType() (string, embedded.LuaTypeFunc) {
	return StickerConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(StickerConfigLuaTypeName)
		L.SetGlobal(StickerConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newStickerConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), stickerConfigLuaMethods))
	}
}

func newStickerConfig(L *lua.LState) int {
	cfg := telegram.StickerConfig{StickerConfig: tgbotapi.StickerConfig{BaseFile: tgbotapi.BaseFile{UseExisting: true}}}
	if L.GetTop() == 2 {
		cfg = telegram.NewStickerShare(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, StickerConfigLuaTypeName)(L))
	return 1
}
