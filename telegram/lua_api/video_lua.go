package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const VideoLuaTypeName = "telegram_video"

var videoLuaMethods = map[string]lua.LGFunction{
	"id":        luaVideoGetFileID,
	"fileSize":  luaVideoGetFileSize,
	"duration":  luaVideoGetDuration,
	"mimeType":  luaVideoGetMimeType,
	"width":     luaVideoGetWidth,
	"height":    luaVideoGetHeight,
	"thumbnail": luaVideoGetThumbnail,
}

func RegisterVideoType() (string, embedded.LuaTypeFunc) {
	return VideoLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(VideoLuaTypeName)
		L.SetGlobal(VideoLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), videoLuaMethods))
	}
}

func checkVideo(L *lua.LState) *tgbotapi.Video {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tgbotapi.Video); ok {
		return v
	}
	L.ArgError(1, VideoLuaTypeName+" expected")
	return nil
}

func luaVideoGetFileID(L *lua.LState) int {
	video := checkVideo(L)
	L.Push(lua.LString(video.FileID))
	return 1
}

func luaVideoGetFileSize(L *lua.LState) int {
	video := checkVideo(L)
	L.Push(lua.LNumber(video.FileSize))
	return 1
}

func luaVideoGetDuration(L *lua.LState) int {
	video := checkVideo(L)
	L.Push(lua.LNumber(video.Duration))
	return 1
}

func luaVideoGetMimeType(L *lua.LState) int {
	video := checkVideo(L)
	L.Push(lua.LString(video.MimeType))
	return 1
}

func luaVideoGetWidth(L *lua.LState) int {
	video := checkVideo(L)
	L.Push(lua.LNumber(video.Width))
	return 1
}

func luaVideoGetHeight(L *lua.LState) int {
	video := checkVideo(L)
	L.Push(lua.LNumber(video.Height))
	return 1
}

func luaVideoGetThumbnail(L *lua.LState) int {
	video := checkVideo(L)
	if video.Thumbnail == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(video.Thumbnail, PhotoLuaTypeName)(L))
	}
	return 1
}
