local d = document_config.new(123456789, "testID")
assert(d:chatId() == 123456789)
assert(d:fileId() == "testID")

d:caption("testing")
assert(d:caption() == "testing")
d:parseMode("HTML")
assert(d:parseMode() == "HTML")