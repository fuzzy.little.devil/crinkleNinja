package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const VideoConfigLuaTypeName = "video_config"

var videoConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fileId":              luaBaseFileFileID,
	"useExisting":         luaBaseFileUseExisting,
	"caption":             luaVideoConfigCaption,
	"parseMode":           luaVideoConfigParseMode,
	"duration":            luaVideoConfigDuration,
}

func RegisterVideoConfigType() (string, embedded.LuaTypeFunc) {
	return VideoConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(VideoConfigLuaTypeName)
		L.SetGlobal(VideoConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newVideoConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), videoConfigLuaMethods))
	}
}

func newVideoConfig(L *lua.LState) int {
	cfg := telegram.VideoConfig{VideoConfig: tgbotapi.VideoConfig{BaseFile: tgbotapi.BaseFile{UseExisting: true}}}
	if L.GetTop() == 2 {
		cfg = telegram.NewVideoShare(int64(L.CheckNumber(1)), L.CheckString(2))
	}
	L.Push(AsLuaType(&cfg, VideoConfigLuaTypeName)(L))
	return 1
}

func checkVideoConfig(L *lua.LState) *telegram.VideoConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.VideoConfig)
	if !ok {
		L.ArgError(1, VideoConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaVideoConfigCaption(L *lua.LState) int {
	v := checkVideoConfig(L)
	if L.GetTop() == 2 {
		v.Caption = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.Caption))
	return 1
}

func luaVideoConfigParseMode(L *lua.LState) int {
	v := checkVideoConfig(L)
	if L.GetTop() == 2 {
		v.ParseMode = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(v.ParseMode))
	return 1
}

func luaVideoConfigDuration(L *lua.LState) int {
	v := checkVideoConfig(L)
	if L.GetTop() == 2 {
		v.Duration = int(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(v.Duration))
	return 1
}
