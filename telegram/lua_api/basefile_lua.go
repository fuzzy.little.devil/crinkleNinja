package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"reflect"
)

func checkBaseFile(L *lua.LState) *tgbotapi.BaseFile {
	value := reflect.ValueOf(L.CheckUserData(1).Value).Elem().FieldByName("BaseFile")
	if ret, ok := value.Addr().Interface().(*tgbotapi.BaseFile); ok {
		return ret
	}
	L.ArgError(1, "expected a base file object")
	return nil
}

func luaBaseFileFileID(L *lua.LState) int {
	c := checkBaseFile(L)
	if L.GetTop() == 2 {
		c.FileID = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(c.FileID))
	return 1
}

func luaBaseFileUseExisting(L *lua.LState) int {
	c := checkBaseFile(L)
	if L.GetTop() == 2 {
		c.UseExisting = L.CheckBool(2)
		return 0
	}
	L.Push(lua.LBool(c.UseExisting))
	return 1
}
