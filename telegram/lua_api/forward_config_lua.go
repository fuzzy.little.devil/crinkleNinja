package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
)

const ForwardConfigLuaTypeName = "forward_config"

var forwardConfigLuaMethods = map[string]lua.LGFunction{
	"chatId":              luaBaseChatChatID,
	"channelUsername":     luaBaseChatChannelUsername,
	"replyToMessageId":    luaBaseChatReplyToMessageID,
	"disableNotification": luaBaseChatDisableNotification,
	"fromChatId":          luaForwardConfigFromChatID,
	"fromChannelUsername": luaForwardConfigFromChannelUsername,
	"messageId":           luaForwardConfigMessageID,
}

func RegisterForwardConfigType() (string, embedded.LuaTypeFunc) {
	return StickerConfigLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(ForwardConfigLuaTypeName)
		L.SetGlobal(ForwardConfigLuaTypeName, mt)
		L.SetField(mt, "new", L.NewFunction(newForwardConfig))
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), forwardConfigLuaMethods))
	}
}

func newForwardConfig(L *lua.LState) int {
	cfg := telegram.ForwardConfig{}
	if L.GetTop() == 3 {
		cfg = telegram.NewForward(int64(L.CheckNumber(1)), int64(L.CheckNumber(2)), int(L.CheckNumber(3)))
	}
	L.Push(AsLuaType(&cfg, ForwardConfigLuaTypeName)(L))
	return 1
}

func checkForwardConfig(L *lua.LState) *telegram.ForwardConfig {
	ud := L.CheckUserData(1)
	v, ok := ud.Value.(*telegram.ForwardConfig)
	if !ok {
		L.ArgError(1, ForwardConfigLuaTypeName+" expected")
		return nil
	}
	return v
}

func luaForwardConfigFromChatID(L *lua.LState) int {
	fc := checkForwardConfig(L)
	if L.GetTop() == 2 {
		fc.FromChatID = int64(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(fc.FromChatID))
	return 1
}

func luaForwardConfigFromChannelUsername(L *lua.LState) int {
	fc := checkForwardConfig(L)
	if L.GetTop() == 2 {
		fc.FromChannelUsername = L.CheckString(2)
		return 0
	}
	L.Push(lua.LString(fc.FromChannelUsername))
	return 1
}

func luaForwardConfigMessageID(L *lua.LState) int {
	fc := checkForwardConfig(L)
	if L.GetTop() == 2 {
		fc.MessageID = int(L.CheckNumber(2))
		return 0
	}
	L.Push(lua.LNumber(fc.MessageID))
	return 1
}
