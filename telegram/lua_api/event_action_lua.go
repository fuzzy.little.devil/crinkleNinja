package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
)

const EventActionLuaTypeName = "event_action"

var eventActionLuaMethods = map[string]lua.LGFunction{
	"name":          luaEventActionGetName,
	"setContext":    luaEventActionSetContext,
	"clearContext":  luaEventActionClearContext,
	"hasContext":    luaEventActionHasContext,
	"endingContext": luaEventActionEndingContext,
}

func RegisterEventActionType() (string, embedded.LuaTypeFunc) {
	return EventActionLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(EventActionLuaTypeName)
		L.SetGlobal(EventActionLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), eventActionLuaMethods))
	}
}

func checkEventAction(L *lua.LState) *events.EventAction {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*events.EventAction); ok {
		return v
	}
	L.ArgError(1, EventActionLuaTypeName+" expected")
	return nil
}

func luaEventActionGetName(L *lua.LState) int {
	ea := checkEventAction(L)
	L.Push(lua.LString(ea.Name))
	return 1
}

func luaEventActionSetContext(L *lua.LState) int {
	ea := checkEventAction(L)
	ea.UpdateContext(model.CurrentUser)
	return 0
}

func luaEventActionClearContext(L *lua.LState) int {
	ea := checkEventAction(L)
	ea.ClearContext(model.CurrentUser)
	return 0
}

func luaEventActionHasContext(L *lua.LState) int {
	ea := checkEventAction(L)
	L.Push(lua.LBool(ea.HasCurrentContext(model.CurrentUser)))
	return 1
}

func luaEventActionEndingContext(L *lua.LState) int {
	ea := checkEventAction(L)
	L.Push(lua.LBool(ea.EndingContext))
	return 1
}
