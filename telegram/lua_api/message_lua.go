package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/util"
	"strings"
)

const MessageLuaTypeName = "telegram_message"

var messageLuaMethods = map[string]lua.LGFunction{
	"chat":                 luaMessageGetChat,
	"date":                 luaMessageGetDate,
	"photo":                luaMessageGetPhoto,
	"audio":                luaMessageGetAudio,
	"document":             luaMessageGetDocument,
	"video":                luaMessageGetVideo,
	"animation":            luaMessageGetAnimation,
	"user":                 luaMessageGetUser,
	"text":                 luaMessageGetText,
	"args":                 luaMessageGetArgs,
	"arg":                  luaMessageGetArg,
	"id":                   luaMessageGetID,
	"timestamp":            luaMessageGetTimestampMillis,
	"fromBotAdmin":         luaIsMessageFromBotAdmin,
	"fromChatAdmin":        luaIsMessageFromChatAdmin,
	"sticker":              luaMessageGetSticker,
	"reply":                luaMessageReply,
	"forwardFromUser":      luaMessageForwardFromUser,
	"forwardFromChat":      luaMessageForwardFromChat,
	"forwardFromMessageId": luaMessageForwardFromMessageID,
}

func RegisterMessageType() (string, embedded.LuaTypeFunc) {
	return MessageLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(MessageLuaTypeName)
		L.SetGlobal(MessageLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), messageLuaMethods))
	}
}

func checkMessage(L *lua.LState) *messaging.Message {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*messaging.Message); ok {
		return v
	}
	L.ArgError(1, MessageLuaTypeName+" expected")
	return nil
}

func luaMessageGetChat(L *lua.LState) int {
	message := checkMessage(L)
	c := model.AddChatFromMessage(message)
	L.Push(AsLuaType(c, ChatLuaTypeName)(L))
	return 1
}

func luaMessageGetUser(L *lua.LState) int {
	message := checkMessage(L)
	u := model.AddUserFromMessage(message)
	L.Push(AsLuaType(u, UserLuaTypeName)(L))
	return 1
}

func luaMessageGetSticker(L *lua.LState) int {
	message := checkMessage(L)
	if message.Sticker == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(message.Sticker, StickerLuaTypeName)(L))
	}
	return 1
}

func luaMessageGetText(L *lua.LState) int {
	message := checkMessage(L)
	L.Push(lua.LString(message.Text))
	return 1
}

func luaMessageGetTimestampMillis(L *lua.LState) int {
	message := checkMessage(L)
	millis := message.Time().UnixNano() / 1000000
	L.Push(lua.LNumber(millis))
	return 1
}

func luaIsMessageFromBotAdmin(L *lua.LState) int {
	message := checkMessage(L)
	bot, ok := L.CheckUserData(2).Value.(telegram.Bottable)
	if !ok {
		L.ArgError(2, BotLuaTypeName+" expected")
	}
	L.Push(lua.LBool(util.InSlice(message.From.ID, bot.AdminIDs())))
	return 1
}

func luaIsMessageFromChatAdmin(L *lua.LState) int {
	message := checkMessage(L)
	chat, ok := L.CheckUserData(2).Value.(*model.Chat)
	if !ok {
		L.ArgError(2, ChatLuaTypeName+" expected")
	}
	_, ok = chat.Admins[message.From.ID]
	L.Push(lua.LBool(ok))
	return 1
}

func luaMessageGetID(L *lua.LState) int {
	message := checkMessage(L)
	L.Push(lua.LNumber(message.MessageID))
	return 1
}

func luaMessageReply(L *lua.LState) int {
	message := checkMessage(L)
	reply := message.Reply()
	if reply == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(AsLuaType(reply, MessageLuaTypeName)(L))
	return 1
}

func luaMessageGetArg(L *lua.LState) int {
	message := checkMessage(L)
	L.Push(lua.LString(message.CommandArguments()))
	return 1
}

func luaMessageGetArgs(L *lua.LState) int {
	message := checkMessage(L)
	tbl := L.NewTable()
	for _, s := range strings.Split(message.CommandArguments(), " ") {
		tbl.Append(lua.LString(s))
	}
	L.Push(tbl)
	return 1
}

func luaMessageForwardFromChat(L *lua.LState) int {
	message := checkMessage(L)
	v := message.ForwardFromChat
	if v == nil {
		L.Push(lua.LNil)
	}
	L.Push(AsLuaType(v, ChatLuaTypeName)(L))
	return 1
}

func luaMessageForwardFromUser(L *lua.LState) int {
	message := checkMessage(L)
	v := message.ForwardFrom
	if v == nil {
		L.Push(lua.LNil)
	}
	L.Push(AsLuaType(v, UserLuaTypeName)(L))
	return 1
}

func luaMessageForwardFromMessageID(L *lua.LState) int {
	message := checkMessage(L)
	L.Push(lua.LNumber(message.ForwardFromMessageID))
	return 1
}

func luaMessageGetDate(L *lua.LState) int {
	message := checkMessage(L)
	L.Push(lua.LNumber(message.Date))
	return 1
}

func luaMessageGetPhoto(L *lua.LState) int {
	message := checkMessage(L)
	tbl := L.NewTable()
	if message.Photo == nil {
		L.Push(lua.LNil)
		return 1
	}
	for _, p := range *message.Photo {
		photo := p
		tbl.Append(AsLuaType(&photo, PhotoLuaTypeName)(L))
	}
	L.Push(tbl)
	return 1
}

func luaMessageGetAudio(L *lua.LState) int {
	message := checkMessage(L)
	if message.Audio == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(message.Audio, AudioLuaTypeName)(L))
	}
	return 1
}

func luaMessageGetDocument(L *lua.LState) int {
	message := checkMessage(L)
	if message.Document == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(message.Document, DocumentLuaTypeName)(L))
	}
	return 1
}

func luaMessageGetVideo(L *lua.LState) int {
	message := checkMessage(L)
	if message.Video == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(message.Video, VideoLuaTypeName)(L))
	}
	return 1
}

func luaMessageGetAnimation(L *lua.LState) int {
	message := checkMessage(L)
	if message.Animation == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(message.Animation, AnimationLuaTypeName)(L))
	}
	return 1
}
