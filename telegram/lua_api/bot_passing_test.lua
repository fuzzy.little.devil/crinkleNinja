-- Doesn't test much, just runs through the code

bot:sendMessage(123456789, "Test", "HTML")
bot:sendSticker(123456789, "testID")
bot:sendPhoto(123456789, "testID")
bot:sendAnimation(123456789, "testID")
bot:sendVideo(123456789, "testID")
local msg = message_config.new(123456789, "test")
bot:send(msg)
bot:replyMessage(message, "test")
bot:replySticker(message, "testID")
bot:deleteMessage(message)
bot:deleteMessage(123456789, 123456789)
bot:getFileURL("testID")
bot:id()
bot:username()

local sent = bot:sendMessage(123456789, "Test", "HTML")
if sent ~= nil then print(sent:text()) end