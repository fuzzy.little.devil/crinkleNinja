package lua_api

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gopkg.in/telegram-bot-api.v4"
)

const BotLuaTypeName = "telegram_bot"

var botLuaMethods = map[string]lua.LGFunction{
	"sendMessage":   luaSendTextMessage,
	"sendSticker":   luaSendStickerMessage,
	"sendPhoto":     luaSendPhotoMessage,
	"sendAnimation": luaSendAnimationMessage,
	"sendVideo":     luaSendVideoMessage,
	"send":          luaSend,
	"replyMessage":  luaReplyToMessageText,
	"replySticker":  luaReplyToMessageSticker,
	"deleteMessage": luaDeleteMessage,
	"getFileURL":    luaGetFileURL,
	"id":            luaGetBotID,
	"username":      luaGetBotUsername,
}

func RegisterBotType() (string, embedded.LuaTypeFunc) {
	return BotLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(BotLuaTypeName)
		L.SetGlobal(BotLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), botLuaMethods))
	}
}

func checkBot(L *lua.LState) telegram.Bottable {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(telegram.Bottable); ok {
		return v
	}
	L.ArgError(1, BotLuaTypeName+" expected")
	return nil
}

func luaSendTextMessage(L *lua.LState) int {
	bot := checkBot(L)
	chatId := L.CheckNumber(2)
	msg := L.CheckAny(3)
	parseMode := ""
	if L.GetTop() == 4 {
		parseMode = L.CheckString(4)
	}
	toSend := telegram.NewMessage(int64(chatId), msg.String())
	toSend.ParseMode = parseMode
	sent, err := bot.Send(toSend)
	if err != nil {
		L.RaiseError("error sending message: %s", err.Error())
	}
	L.Push(AsLuaType(messaging.NewMessage(&sent), MessageLuaTypeName)(L))
	return 1
}

func sendMedia(L *lua.LState, f telegram.ChatConfigFunc) {
	bot := checkBot(L)
	chatId := L.CheckNumber(2)
	id := L.CheckString(3)
	cfg := f(int64(chatId), id)
	sent, err := bot.Send(cfg.Config())
	if err != nil {
		L.RaiseError("error sending %s: %s", cfg.Name(), err.Error())
	}
	L.Push(AsLuaType(messaging.NewMessage(&sent), MessageLuaTypeName)(L))
}

func luaSendStickerMessage(L *lua.LState) int {
	sendMedia(L, telegram.NewStickerChattable)
	return 1
}

func luaSendPhotoMessage(L *lua.LState) int {
	sendMedia(L, telegram.NewPhotoChattable)
	return 1
}

func luaSendAnimationMessage(L *lua.LState) int {
	sendMedia(L, telegram.NewAnimationChattable)
	return 1
}

func luaSendVideoMessage(L *lua.LState) int {
	sendMedia(L, telegram.NewVideoChattable)
	return 1
}

func luaReplyToMessageText(L *lua.LState) int {
	bot := checkBot(L)
	oldMsg, ok := L.CheckUserData(2).Value.(*messaging.Message)
	if !ok || oldMsg == nil {
		L.ArgError(2, MessageLuaTypeName+" expected")
		return 0
	}
	msg := telegram.NewMessage(oldMsg.Chat.ID, L.CheckString(3))
	parseMode := ""
	if L.GetTop() == 4 {
		parseMode = L.CheckString(4)
	}
	msg.ParseMode = parseMode
	msg.ReplyToMessageID = oldMsg.MessageID
	sent, err := bot.Send(msg)
	if err != nil {
		L.RaiseError("error sending reply: %s", err.Error())
	}
	L.Push(AsLuaType(messaging.NewMessage(&sent), MessageLuaTypeName)(L))
	return 1
}

func luaReplyToMessageSticker(L *lua.LState) int {
	bot := checkBot(L)
	oldMsg, ok := L.CheckUserData(2).Value.(*messaging.Message)
	if !ok || oldMsg == nil {
		L.ArgError(2, MessageLuaTypeName+" expected")
		return 0
	}
	msg := telegram.NewStickerShare(oldMsg.Chat.ID, L.CheckString(3))
	msg.ReplyToMessageID = oldMsg.MessageID
	sent, err := bot.Send(msg)
	if err != nil {
		L.RaiseError("error sending reply: %s", err.Error())
	}
	L.Push(AsLuaType(messaging.NewMessage(&sent), MessageLuaTypeName)(L))
	return 1
}

func luaDeleteMessage(L *lua.LState) int {
	bot := checkBot(L)
	var chatId int64
	var msgId int
	if L.GetTop() == 3 {
		chatId = L.CheckInt64(2)
		msgId = L.CheckInt(3)
	} else {
		msg, ok := L.CheckUserData(2).Value.(*messaging.Message)
		if !ok || msg == nil {
			L.ArgError(2, MessageLuaTypeName+" expected")
			return 0
		}
		chatId = msg.Chat.ID
		msgId = msg.MessageID
	}
	if _, err := bot.DeleteMessage(tgbotapi.DeleteMessageConfig{ChatID: chatId, MessageID: msgId}); err != nil {
		L.RaiseError("error deleting message: %s", err.Error())
	}
	return 0
}

func luaSend(L *lua.LState) int {
	bot := checkBot(L)
	cfg, ok := L.CheckUserData(2).Value.(telegram.ChattableConfig)
	if !ok {
		L.ArgError(2, "expected chattable config type")
		return 0
	}
	sent, err := bot.Send(cfg.Config())
	if err != nil {
		L.RaiseError("error sending '%s' message: %s", cfg.Name(), err.Error())
	}
	L.Push(AsLuaType(messaging.NewMessage(&sent), MessageLuaTypeName)(L))
	return 1
}

func luaGetFileURL(L *lua.LState) int {
	bot := checkBot(L)
	id := L.CheckString(2)
	u, err := bot.GetFileDirectURL(id)
	if err != nil {
		L.RaiseError("error getting file URL: %s", err.Error())
		return 0
	}
	L.Push(lua.LString(u))
	return 1
}

func luaGetBotID(L *lua.LState) int {
	bot := checkBot(L)
	L.Push(lua.LNumber(bot.ID()))
	return 1
}

func luaGetBotUsername(L *lua.LState) int {
	bot := checkBot(L)
	L.Push(lua.LString(bot.UserName()))
	return 1
}
