package lua_api

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const StickerLuaTypeName = "telegram_sticker"

var stickerLuaMethods = map[string]lua.LGFunction{
	"id":        luaStickerGetFileID,
	"emoji":     luaStickerGetEmoji,
	"fileSize":  luaStickerGetFileSize,
	"height":    luaStickerGetHeight,
	"width":     luaStickerGetWidth,
	"thumbnail": luaStickerGetThumbnail,
	"setName":   luaStickerGetSetName,
}

func RegisterStickerType() (string, embedded.LuaTypeFunc) {
	return StickerLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(StickerLuaTypeName)
		L.SetGlobal(StickerLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), stickerLuaMethods))
	}
}

func checkSticker(L *lua.LState) *tgbotapi.Sticker {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tgbotapi.Sticker); ok {
		return v
	}
	L.ArgError(1, StickerLuaTypeName+" expected")
	return nil
}

func luaStickerGetFileID(L *lua.LState) int {
	sticker := checkSticker(L)
	L.Push(lua.LString(sticker.FileID))
	return 1
}

func luaStickerGetEmoji(L *lua.LState) int {
	sticker := checkSticker(L)
	L.Push(lua.LString(sticker.Emoji))
	return 1
}

func luaStickerGetFileSize(L *lua.LState) int {
	sticker := checkSticker(L)
	L.Push(lua.LNumber(sticker.FileSize))
	return 1
}

func luaStickerGetHeight(L *lua.LState) int {
	sticker := checkSticker(L)
	L.Push(lua.LNumber(sticker.Height))
	return 1
}

func luaStickerGetWidth(L *lua.LState) int {
	sticker := checkSticker(L)
	L.Push(lua.LNumber(sticker.Width))
	return 1
}

func luaStickerGetThumbnail(L *lua.LState) int {
	sticker := checkSticker(L)
	if sticker.Thumbnail == nil {
		L.Push(lua.LNil)
	} else {
		L.Push(AsLuaType(sticker.Thumbnail, PhotoLuaTypeName)(L))
	}
	return 1
}

func luaStickerGetSetName(L *lua.LState) int {
	sticker := checkSticker(L)
	L.Push(lua.LString(sticker.SetName))
	return 1
}
