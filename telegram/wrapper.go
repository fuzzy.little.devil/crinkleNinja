package telegram

import "gopkg.in/telegram-bot-api.v4"

// This file wraps implementations from the bot API to make it easier to upgrade or replace parts later

// Re-implement chattable interface for expandability
type ChattableConfig interface {
	Name() string
	Config() tgbotapi.Chattable
}

type ChatConfigFunc func(chatID int64, fileID string) ChattableConfig

// Text message
type MessageConfig struct {
	tgbotapi.MessageConfig
}

func (m MessageConfig) Name() string {
	return "message"
}

func (m MessageConfig) Config() tgbotapi.Chattable {
	return m.MessageConfig
}

func NewMessage(chatID int64, text string) MessageConfig {
	return MessageConfig{MessageConfig: tgbotapi.NewMessage(chatID, text)}
}

// Forward Message
type ForwardConfig struct {
	tgbotapi.ForwardConfig
}

func (f ForwardConfig) Name() string {
	return "forward"
}

func (f ForwardConfig) Config() tgbotapi.Chattable {
	return f.ForwardConfig
}

func NewForward(fromChatID, toChatID int64, messageID int) ForwardConfig {
	return ForwardConfig{ForwardConfig: tgbotapi.NewForward(toChatID, fromChatID, messageID)}
}

// Chat Action
type ChatActionConfig struct {
	tgbotapi.ChatActionConfig
}

func (c ChatActionConfig) Name() string {
	return "chat_action"
}

func (c ChatActionConfig) Config() tgbotapi.Chattable {
	return c.ChatActionConfig
}

func NewChatAction(chatID int64, action string) ChatActionConfig {
	return ChatActionConfig{ChatActionConfig: tgbotapi.NewChatAction(chatID, action)}
}

// Sticker Message
type StickerConfig struct {
	tgbotapi.StickerConfig
}

func (s StickerConfig) Name() string {
	return "sticker"
}

func (s StickerConfig) Config() tgbotapi.Chattable {
	return s.StickerConfig
}

func NewStickerShare(chatID int64, fileID string) StickerConfig {
	return StickerConfig{StickerConfig: tgbotapi.NewStickerShare(chatID, fileID)}
}

func NewStickerChattable(chatID int64, fileID string) ChattableConfig {
	return StickerConfig{StickerConfig: tgbotapi.NewStickerShare(chatID, fileID)}
}

// Photo Message
type PhotoConfig struct {
	tgbotapi.PhotoConfig
}

func (p PhotoConfig) Name() string {
	return "photo"
}

func (p PhotoConfig) Config() tgbotapi.Chattable {
	return p.PhotoConfig
}

func NewPhotoChattable(chatID int64, fileID string) ChattableConfig {
	return PhotoConfig{PhotoConfig: tgbotapi.NewPhotoShare(chatID, fileID)}
}

func NewPhotoShare(chatID int64, fileID string) PhotoConfig {
	return PhotoConfig{PhotoConfig: tgbotapi.NewPhotoShare(chatID, fileID)}
}

// Audio Message
type AudioConfig struct {
	tgbotapi.AudioConfig
}

func (a AudioConfig) Name() string {
	return "audio"
}

func (a AudioConfig) Config() tgbotapi.Chattable {
	return a.AudioConfig
}

func NewAudioShare(chatID int64, fileID string) AudioConfig {
	return AudioConfig{AudioConfig: tgbotapi.NewAudioShare(chatID, fileID)}
}

// Document Message
type DocumentConfig struct {
	tgbotapi.DocumentConfig
}

func (d DocumentConfig) Name() string {
	return "document"
}

func (d DocumentConfig) Config() tgbotapi.Chattable {
	return d.DocumentConfig
}

func NewDocumentShare(chatID int64, fileID string) DocumentConfig {
	return DocumentConfig{DocumentConfig: tgbotapi.NewDocumentShare(chatID, fileID)}
}

// Video Message
type VideoConfig struct {
	tgbotapi.VideoConfig
}

func (v VideoConfig) Name() string {
	return "video"
}

func (v VideoConfig) Config() tgbotapi.Chattable {
	return v.VideoConfig
}

func NewVideoChattable(chatID int64, fileID string) ChattableConfig {
	return VideoConfig{VideoConfig: tgbotapi.NewVideoShare(chatID, fileID)}
}

func NewVideoShare(chatID int64, fileID string) VideoConfig {
	return VideoConfig{VideoConfig: tgbotapi.NewVideoShare(chatID, fileID)}
}

// Animation Message
type AnimationConfig struct {
	tgbotapi.AnimationConfig
}

func (a AnimationConfig) Name() string {
	return "animation"
}

func (a AnimationConfig) Config() tgbotapi.Chattable {
	return a.AnimationConfig
}

func NewAnimationChattable(chatID int64, fileID string) ChattableConfig {
	return AnimationConfig{AnimationConfig: tgbotapi.NewAnimationShare(chatID, fileID)}
}

func NewAnimationShare(chatID int64, fileID string) AnimationConfig {
	return AnimationConfig{AnimationConfig: tgbotapi.NewAnimationShare(chatID, fileID)}
}
