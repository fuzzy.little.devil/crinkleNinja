-- string builder test

local sb = require("string.builder")
local s = sb.new()
s:append("test")
s:append("lua")
assert(s:string() == "testlua", "expected s:string() to equal 'testlua'")