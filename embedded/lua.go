package embedded

import (
	"context"
	"fmt"
	"github.com/yuin/gluare"
	"github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"layeh.com/gopher-json"
	"log"
	"sync"
)

type LuaTypeFunc func(L *lua.LState)
type ModuleRegisterFunc func() (string, lua.LGFunction)
type TypeRegisterFunc func() (string, LuaTypeFunc)
type LuaValueFunc func(L *lua.LState) lua.LValue

type LuaConfig struct {
	Script     string
	Globals    map[string]LuaValueFunc
	State      *lua.LState
	Modules    []ModuleRegisterFunc
	Types      []TypeRegisterFunc
	Context    context.Context
	CancelFunc context.CancelFunc
}

func luaPrint(L *lua.LState) int {
	n := L.GetTop()
	str := ""
	for i := 1; i <= n; i++ {
		if i > 1 {
			str += "	"
		}
		str += L.CheckAny(i).String()
	}
	output += str + "\n"
	return 0
}

func luaLog(L *lua.LState) int {
	n := L.GetTop()
	str := ""
	for i := 1; i <= n; i++ {
		if i > 1 {
			str += "	"
		}
		str += L.CheckAny(i).String()
	}
	log.Println(str)
	return 0
}

var lock = sync.Mutex{}
var output = ""

var defaultModules = []ModuleRegisterFunc{
	// regex functionality
	func() (name string, loader lua.LGFunction) {
		return "re", gluare.Loader
	},
	func() (name string, loader lua.LGFunction) {
		return "json", json.Loader
	},
	StringBuilderModuleRegister,
	MarkovChainModuleRegister,
	TableUtilsModuleRegister,
}

var defaultTypes = []TypeRegisterFunc{
	RegisterStringBuilderType,
	RegisterMarkovChainType,
}

func RunLua(luaConfig *LuaConfig) (string, error) {
	lock.Lock()
	output = ""
	defer lock.Unlock()
	if luaConfig.State == nil {
		luaConfig.State = lua.NewState()
	}
	defer luaConfig.State.Close()
	for _, r := range defaultModules {
		k, v := r()
		config.LogDebug("Preloading default Lua module '%s'\n", k)
		luaConfig.State.PreloadModule(k, v)
	}
	for _, r := range defaultTypes {
		k, v := r()
		config.LogDebug("Registering default lua type '%s'\n", k)
		v(luaConfig.State)
	}
	for _, r := range luaConfig.Modules {
		k, v := r()
		config.LogDebug("Preloading Lua module '%s'\n", k)
		luaConfig.State.PreloadModule(k, v)
	}
	for _, r := range luaConfig.Types {
		k, v := r()
		config.LogDebug("Registering Lua type '%s'\n", k)
		v(luaConfig.State)
	}
	config.LogDebug("Running Script:\n%s", luaConfig.Script)
	for k, v := range luaConfig.Globals {
		luaConfig.State.SetGlobal(k, v(luaConfig.State))
	}
	luaConfig.State.SetGlobal("print", luaConfig.State.NewFunction(luaPrint))
	luaConfig.State.SetGlobal("log", luaConfig.State.NewFunction(luaLog))
	if luaConfig.Context == nil {
		return "", fmt.Errorf("context is required")
	}
	luaConfig.State.SetContext(luaConfig.Context)
	err := luaConfig.State.DoString(luaConfig.Script)
	luaConfig.CancelFunc()
	return output, err
}
