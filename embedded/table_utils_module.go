package embedded

import (
	lua "github.com/yuin/gopher-lua"
)

const TableUtilsModuleName = "table.utils"

var tableUtilsModuleExports = map[string]lua.LGFunction{
	"slice":    tableSlice,
	"contains": tableContains,
	"merge":    tableMerge,
}

func TableUtilsModuleLoader(L *lua.LState) int {
	mod := L.SetFuncs(L.NewTable(), tableUtilsModuleExports)
	L.SetField(mod, "name", lua.LString(TableUtilsModuleName))
	L.Push(mod)
	return 1
}

func TableUtilsModuleRegister() (string, lua.LGFunction) {
	return TableUtilsModuleName, TableUtilsModuleLoader
}

func tableSlice(L *lua.LState) int {
	tbl := L.CheckTable(1)
	start := L.CheckInt(2)
	end := L.OptInt(3, tbl.Len())
	retainKeys := L.OptBool(4, false)
	counter := 1
	lTable := new(lua.LTable)
	tbl.ForEach(func(k, v lua.LValue) {
		if counter >= start && counter <= end {
			if retainKeys {
				lTable.RawSet(k, v)
			} else {
				lTable.Append(v)
			}
		}
		counter++
	})
	L.Push(lTable)
	return 1
}

func tableContains(L *lua.LState) int {
	tbl := L.CheckTable(1)
	needle := L.CheckAny(2)
	found := false
	for i := 1; i <= tbl.Len(); i++ {
		if needle.String() == tbl.RawGetInt(i).String() {
			found = true
			break
		}
	}
	L.Push(lua.LBool(found))
	return 1
}

func tableMerge(L *lua.LState) int {
	dst := L.CheckTable(1)
	src := L.CheckTable(2)
	src.ForEach(func(_, v lua.LValue) {
		dst.Append(v)
	})
	return 0
}
