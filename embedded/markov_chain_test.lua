local markov = require("markov.chain")

inputNames = {
    "Frodo",
    "Bilbo",
    "Baggins",
    "Gimli",
    "Gandalf",
    "Pippin",
    "Merry",
    "Legolas",
    "Aragorn",
    "Arathorn",
    "Boromir",
    "Faramir",
    "Elrond",
    "Gollum",
    "Samwise",
    "Sauron",
    "Saruman",
    "Ellendil",
    "Treebeard",
    "Sackville",
    "Radagast",
    "Tom",
    "Bombadil",
    "Arwen",
    "Eowyn",
}
local chain = markov.new(2)
for _, name in pairs(inputNames) do chain:add(name) end

local s = chain:getText(true)
if s ~= nil then print(s) else print("Unable to getText. Try lowering the chain order") end

local table = require("table")
function slice(T, first, last, step)
    local sliced = {}
    for i = first or 1, last or #T, step or 1 do
        sliced[#sliced+1] = T[i]
    end
    return sliced
end
local order = chain:order()
local tokens = {}
for i=1, order do tokens[i] = markov.startToken() end
while tokens[#tokens-1] ~= markov.endToken() do
    local str = slice(tokens, #tokens+1 - order, #tokens)
    local next = chain:generate(str)
    table.insert(tokens, next)
end

print(table.concat(slice(tokens, order+1, #tokens-2), ""))