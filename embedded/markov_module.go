package embedded

import (
	"fmt"
	"github.com/mb-14/gomarkov"
	lua "github.com/yuin/gopher-lua"
	"strings"
)

const MarkovChainModuleName = "markov.chain"

var markovChainModuleExports = map[string]lua.LGFunction{
	"new":        markovNewChain,
	"startToken": markovStartToken,
	"endToken":   markovEndToken,
}

func MarkovChainModuleLoader(L *lua.LState) int {
	mod := L.SetFuncs(L.NewTable(), markovChainModuleExports)
	L.SetField(mod, "name", lua.LString(MarkovChainModuleName))
	L.Push(mod)
	return 1
}

func MarkovChainModuleRegister() (string, lua.LGFunction) {
	return MarkovChainModuleName, MarkovChainModuleLoader
}

type Chain struct {
	*gomarkov.Chain
	Input     map[string]bool
	iteration int
}

func (c *Chain) GetText() string {
	order := c.Order
	tokens := make([]string, 0)
	for i := 0; i < order; i++ {
		tokens = append(tokens, gomarkov.StartToken)
	}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := c.Generate(tokens[(len(tokens) - order):])
		tokens = append(tokens, next)
	}
	s := strings.Join(tokens[order:len(tokens)-1], "")
	return s
}

func (c *Chain) AddString(s string) {
	c.Input[s] = true
	c.Add(strings.Split(s, ""))
}

func markovNewChain(L *lua.LState) int {
	order := L.CheckInt(1)
	chain := &Chain{
		Chain: gomarkov.NewChain(order),
		Input: map[string]bool{},
	}
	ud := L.NewUserData()
	ud.Value = chain
	L.SetMetatable(ud, L.GetTypeMetatable(MarkovChainLuaTypeName))
	L.Push(ud)
	return 1
}

func markovStartToken(L *lua.LState) int {
	L.Push(lua.LString(gomarkov.StartToken))
	return 1
}

func markovEndToken(L *lua.LState) int {
	L.Push(lua.LString(gomarkov.EndToken))
	return 1
}

var MarkovChainLuaTypeName = "markov_chain"

var markovChainLuaMethods = map[string]lua.LGFunction{
	"add":      markovChainAdd,
	"generate": markovChainGenerate,
	"order":    markovChainOrder,
	"getText":  markovChainGetText,
}

func RegisterMarkovChainType() (string, LuaTypeFunc) {
	return MarkovChainLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(MarkovChainLuaTypeName)
		L.SetGlobal(MarkovChainLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), markovChainLuaMethods))
	}
}

func checkMarkovChain(L *lua.LState) *Chain {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*Chain); ok {
		return v
	}
	L.ArgError(1, MarkovChainLuaTypeName+" expected")
	return nil
}

func markovChainAdd(L *lua.LState) int {
	chain := checkMarkovChain(L)
	chain.AddString(L.CheckString(2))
	return 0
}

func tableToStringSlice(table *lua.LTable) (slice []string, err error) {
	table.ForEach(func(_, v lua.LValue) {
		sValue, isStr := v.(lua.LString)
		nValue, isNum := v.(lua.LNumber)
		_, isMap := v.(*lua.LTable)
		if isMap {
			err = fmt.Errorf("map detected")
			return
		}
		if isStr {
			slice = append(slice, sValue.String())
		}
		if isNum {
			slice = append(slice, nValue.String())
		}
	})
	return
}

func markovChainGenerate(L *lua.LState) int {
	chain := checkMarkovChain(L)
	slice, err := tableToStringSlice(L.CheckTable(2))
	if err != nil {
		L.RaiseError("unexpected table, expected table of strings")
		return 0
	}
	text, err := chain.Generate(slice)
	if err != nil {
		L.RaiseError("error executing markov generate: %s", err.Error())
		return 0
	}
	L.Push(lua.LString(text))
	return 1
}

func markovChainGetText(L *lua.LState) int {
	chain := checkMarkovChain(L)
	checkList := L.OptBool(2, false)
	minLength := L.OptInt(3, 2)
	count := 0
	var s string
	for {
		count++
		if count > 10000 {
			L.Push(lua.LNil)
			return 1
		}
		s = chain.GetText()
		if checkList {
			if _, ok := chain.Input[s]; ok {
				continue
			}
		}
		if len(s) < minLength {
			continue
		}
		break
	}
	L.Push(lua.LString(s))
	return 1
}

func markovChainOrder(L *lua.LState) int {
	chain := checkMarkovChain(L)
	L.Push(lua.LNumber(chain.Order))
	return 1
}
