local tableUtils = require("table.utils")
local table = require("table")

local a = {"one", "two", "three", "four", "five"}
local b = tableUtils.slice(a, 2, 4, true)
local c = tableUtils.slice(a, 2, 4)
assert(b[1] == nil, "expected nil")
assert(b[2] == "two", "expected 'two'")
assert(c[1] == "two", "expected 'two'")
local s = table.concat(c, " ")
assert(s == "two three four", "expected 'two three four'")
a = {{"one", "two"}, "three", "four", "five"}
b = tableUtils.slice(a, 1, 2, true)
assert(b[1][2] == "two", "expected 'two'")

a = {1, 2, 3, "four", "five", true}
assert(tableUtils.contains(a, 2), "expected 2 in table")
assert(tableUtils.contains(a, "four"), "expected 'four' in table")
assert(tableUtils.contains(a, true), "expected true in table")

a = {"one", "two", "three"}
b = {"four", "five", "six"}
tableUtils.merge(a, b)
assert(tableUtils.contains(a, "six"), "expected 'six' to be in table")