package embedded_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestEmbeddedLua(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Embedded Lua Suite")
}
