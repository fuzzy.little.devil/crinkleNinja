package embedded_test

import (
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/embedded"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

var _ = Describe("Embedded Lua", func() {
	Describe("Script", func() {
		var scriptFiles []string
		err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
			if filepath.Ext(path) == ".lua" {
				scriptFiles = append(scriptFiles, path)
			}
			return nil
		})
		if err != nil {
			panic(err)
		}
		if len(scriptFiles) == 0 {
			Fail("no scripts?!")
		}
		for _, file := range scriptFiles {
			b, err := ioutil.ReadFile(file)
			if err != nil {
				panic(err)
			}
			luaState := lua.NewState(lua.Options{
				IncludeGoStackTrace: true,
			})
			ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
			luaConfig := &embedded.LuaConfig{
				Script:     string(b),
				State:      luaState,
				Context:    ctx,
				CancelFunc: cancel,
			}
			_, err = embedded.RunLua(luaConfig)
			Context("With script "+file, func() {
				It("should not error", func() {
					Expect(err).Should(BeNil())
				})
			})
		}
	})
})
