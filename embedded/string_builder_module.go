package embedded

import (
	lua "github.com/yuin/gopher-lua"
)

const StringBuilderModuleName = "string.builder"

var sbModuleExports = map[string]lua.LGFunction{
	"new": sbNewBuilder,
}

func StringBuilderModuleLoader(L *lua.LState) int {
	mod := L.SetFuncs(L.NewTable(), sbModuleExports)
	L.SetField(mod, "name", lua.LString(StringBuilderModuleName))
	L.Push(mod)
	return 1
}

func StringBuilderModuleRegister() (string, lua.LGFunction) {
	return StringBuilderModuleName, StringBuilderModuleLoader
}

func sbNewBuilder(L *lua.LState) int {
	sb := &StringBuilder{}
	ud := L.NewUserData()
	ud.Value = sb
	L.SetMetatable(ud, L.GetTypeMetatable(StringBuilderLuaTypeName))
	L.Push(ud)
	return 1
}

type StringBuilder struct {
	buffer string
}

func (sb *StringBuilder) Append(s string) {
	sb.buffer += s
}

func (sb *StringBuilder) String() string {
	return sb.buffer
}

var StringBuilderLuaTypeName = "string_builder"

var sbLuaMethods = map[string]lua.LGFunction{
	"append": sbAppend,
	"string": sbString,
}

func RegisterStringBuilderType() (string, LuaTypeFunc) {
	return StringBuilderLuaTypeName, func(L *lua.LState) {
		mt := L.NewTypeMetatable(StringBuilderLuaTypeName)
		L.SetGlobal(StringBuilderLuaTypeName, mt)
		L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), sbLuaMethods))
	}
}

func checkStringBuilder(L *lua.LState) *StringBuilder {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*StringBuilder); ok {
		return v
	}
	L.ArgError(1, StringBuilderLuaTypeName+" expected")
	return nil
}

func sbString(L *lua.LState) int {
	sb := checkStringBuilder(L)
	L.Push(lua.LString(sb.String()))
	return 1
}

func sbAppend(L *lua.LState) int {
	sb := checkStringBuilder(L)
	sb.Append(L.CheckString(2))
	return 0
}
