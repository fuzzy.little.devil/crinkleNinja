package database_test

import (
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
)

var _ = Describe("Dynamo Database", func() {
	BeforeEach(func() {
		database.DB = nil
	})
	AfterEach(func() {
		database.DB = nil
	})
	Describe("When initializing database", func() {
		It("should not be nil", func() {
			config.Stage = "dev"
			database.InitializeDatabase()
			Expect(database.DB).ShouldNot(BeNil())
			config.Stage = ""
		})
	})
	Describe("Getting an item from DynamoDB", func() {
		It("should error when there is no database", func() {
			_, err := database.Get(&dynamodb.GetItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on provisioned throughput exceeded", func() {
			database.DB = MockDynamoDB{
				ExpectedError: awserr.New(dynamodb.ErrCodeProvisionedThroughputExceededException, "test", nil),
			}
			_, err := database.Get(&dynamodb.GetItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a result without an error", func() {
			database.DB = MockDynamoDB{
				ExpectedResult: &dynamodb.GetItemOutput{},
			}
			result, err := database.Get(&dynamodb.GetItemInput{})
			Expect(err).Should(BeNil())
			Expect(result).ShouldNot(BeNil())
		})
	})

	Describe("Putting an item into DynamoDB", func() {
		It("should error when there is no database", func() {
			_, err := database.Put(&dynamodb.PutItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on provisioned throughput exceeded", func() {
			database.DB = MockDynamoDB{
				ExpectedError: awserr.New(dynamodb.ErrCodeProvisionedThroughputExceededException, "test", nil),
			}
			_, err := database.Put(&dynamodb.PutItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a result without an error", func() {
			database.DB = MockDynamoDB{
				ExpectedResult: &dynamodb.PutItemOutput{},
			}
			result, err := database.Put(&dynamodb.PutItemInput{})
			Expect(err).Should(BeNil())
			Expect(result).ShouldNot(BeNil())
		})
	})

	Describe("Updating an item in DynamoDB", func() {
		It("should error when there is no database", func() {
			_, err := database.Update(&dynamodb.UpdateItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on provisioned throughput exceeded", func() {
			database.DB = MockDynamoDB{
				ExpectedError: awserr.New(dynamodb.ErrCodeProvisionedThroughputExceededException, "test", nil),
			}
			_, err := database.Update(&dynamodb.UpdateItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a result without an error", func() {
			database.DB = MockDynamoDB{
				ExpectedResult: &dynamodb.UpdateItemOutput{},
			}
			result, err := database.Update(&dynamodb.UpdateItemInput{})
			Expect(err).Should(BeNil())
			Expect(result).ShouldNot(BeNil())
		})
	})

	Describe("Deleting an item from DynamoDB", func() {
		It("should error when there is no database", func() {
			_, err := database.Delete(&dynamodb.DeleteItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on provisioned throughput exceeded", func() {
			database.DB = MockDynamoDB{
				ExpectedError: awserr.New(dynamodb.ErrCodeProvisionedThroughputExceededException, "test", nil),
			}
			_, err := database.Delete(&dynamodb.DeleteItemInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a result without an error", func() {
			database.DB = MockDynamoDB{
				ExpectedResult: &dynamodb.DeleteItemOutput{},
			}
			result, err := database.Delete(&dynamodb.DeleteItemInput{})
			Expect(err).Should(BeNil())
			Expect(result).ShouldNot(BeNil())
		})
	})

	Describe("Querying items from DynamoDB", func() {
		It("should error when there is no database", func() {
			_, err := database.Query(&dynamodb.QueryInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on provisioned throughput exceeded", func() {
			database.DB = MockDynamoDB{
				ExpectedError: awserr.New(dynamodb.ErrCodeProvisionedThroughputExceededException, "test", nil),
			}
			_, err := database.Query(&dynamodb.QueryInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a result without an error", func() {
			database.DB = MockDynamoDB{
				ExpectedResult: &dynamodb.QueryOutput{},
			}
			result, err := database.Query(&dynamodb.QueryInput{})
			Expect(err).Should(BeNil())
			Expect(result).ShouldNot(BeNil())
		})
	})

	Describe("Scanning items from DynamoDB", func() {
		It("should error when there is no database", func() {
			_, err := database.Scan(&dynamodb.ScanInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on provisioned throughput exceeded", func() {
			database.DB = MockDynamoDB{
				ExpectedError: awserr.New(dynamodb.ErrCodeProvisionedThroughputExceededException, "test", nil),
			}
			_, err := database.Scan(&dynamodb.ScanInput{})
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a result without an error", func() {
			database.DB = MockDynamoDB{
				ExpectedResult: &dynamodb.ScanOutput{},
			}
			result, err := database.Scan(&dynamodb.ScanInput{})
			Expect(err).Should(BeNil())
			Expect(result).ShouldNot(BeNil())
		})
	})
})
