package database

import (
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"log"
	"os"
)

var DB dynamodbiface.DynamoDBAPI

func GetTablePrefix() string {
	return fmt.Sprintf("%s-%s", os.Getenv("AWS_LAMBDA_FUNCTION_NAME"), config.Stage)
}

func InitializeDatabase() {
	cfg := &aws.Config{}
	if config.IsDebug() {
		cfg = cfg.WithLogLevel(aws.LogDebugWithRequestErrors)
		cfg = cfg.WithLogLevel(aws.LogDebug)
	}
	sess, err := session.NewSession(cfg)
	if err != nil {
		panic(err)
	}
	DB = dynamodb.New(sess)
}

func Get(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	config.LogDebug("DB Get: %v", input)
	if DB == nil {
		return nil, errors.New("no database")
	}
	result, err := DB.GetItem(input)
	if err != nil {
		log.Printf("database.Get error: %v", err)
		return nil, err
	}
	return result, nil
}

func Put(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	config.LogDebug("DB Put: %v", input)
	if DB == nil {
		return nil, errors.New("no database")
	}
	result, err := DB.PutItem(input)
	if err != nil {
		log.Printf("database.Put error: %v", err)
		return nil, err
	}
	return result, nil
}

func Update(input *dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, error) {
	config.LogDebug("DB Update: %v", input)
	if DB == nil {
		return nil, errors.New("no database")
	}
	result, err := DB.UpdateItem(input)
	if err != nil {
		log.Printf("database.Update error: %v", err)
		return nil, err
	}
	return result, nil
}

func Delete(input *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error) {
	config.LogDebug("DB Delete: %v", input)
	if DB == nil {
		return nil, errors.New("no database")
	}
	result, err := DB.DeleteItem(input)
	if err != nil {
		log.Printf("database.Delete error: %v", err)
		return nil, err
	}
	return result, nil
}

func Query(input *dynamodb.QueryInput) (*dynamodb.QueryOutput, error) {
	if DB == nil {
		return nil, errors.New("no database")
	}
	result, err := DB.Query(input)
	if err != nil {
		log.Printf("database.Query error: %v", err)
		return nil, err
	}
	return result, nil
}

func Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {
	if DB == nil {
		return nil, errors.New("no database")
	}
	result, err := DB.Scan(input)
	if err != nil {
		log.Printf("database.Scan error: %v", err)
		return nil, err
	}
	return result, nil
}
