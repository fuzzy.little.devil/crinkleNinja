package database_test

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

type MockDynamoDB struct {
	dynamodbiface.DynamoDBAPI
	ExpectedError  error
	ExpectedResult interface{}
}

func (m MockDynamoDB) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*dynamodb.GetItemOutput), nil
}

func (m MockDynamoDB) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*dynamodb.PutItemOutput), nil
}

func (m MockDynamoDB) UpdateItem(input *dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*dynamodb.UpdateItemOutput), nil
}

func (m MockDynamoDB) DeleteItem(input *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*dynamodb.DeleteItemOutput), nil
}

func (m MockDynamoDB) Query(input *dynamodb.QueryInput) (*dynamodb.QueryOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*dynamodb.QueryOutput), nil
}

func (m MockDynamoDB) Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*dynamodb.ScanOutput), nil
}
