package util

import (
	"hash/fnv"
	"reflect"
	"regexp"
)

func PtrString(s string) *string {
	return &s
}

func GenerateId(s string) int64 {
	h := fnv.New32a()
	_, _ = h.Write([]byte(s))
	return int64(h.Sum32())
}

func InterfaceToMap(iface interface{}) map[string]string {
	s := reflect.ValueOf(iface)
	if s.Kind() != reflect.Map {
		return nil
	}
	ret := map[string]string{}
	for _, k := range s.MapKeys() {
		ret[k.String()] = s.MapIndex(k).Interface().(string)
	}
	return ret
}

func GetRegexGroups(s string, re *regexp.Regexp) map[string]string {
	matches := re.FindStringSubmatch(s)
	result := make(map[string]string)
	for i, name := range re.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = matches[i]
		}
	}
	return result
}

func InSlice(needle interface{}, haystack interface{}) bool {
	if reflect.TypeOf(haystack).Kind() != reflect.Slice {
		panic("haystack is not a slice")
	}
	h := reflect.ValueOf(haystack)
	for i := 0; i < h.Len(); i++ {
		if reflect.DeepEqual(needle, h.Index(i).Interface()) {
			return true
		}
	}
	return false
}
