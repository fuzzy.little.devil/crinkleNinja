package config_test

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-sdk-go/service/secretsmanager/secretsmanageriface"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
)

type MockSM struct {
	secretsmanageriface.SecretsManagerAPI
	ExpectedError  error
	ExpectedResult interface{}
}

func (m MockSM) GetSecretValue(_ *secretsmanager.GetSecretValueInput) (*secretsmanager.GetSecretValueOutput, error) {
	if m.ExpectedError != nil {
		return nil, m.ExpectedError
	}
	return m.ExpectedResult.(*secretsmanager.GetSecretValueOutput), nil
}

var _ = Describe("Config", func() {
	BeforeEach(func() {
		config.Stage = ""
		config.SecretsManagerClient = nil
		config.Secrets = nil
	})
	AfterEach(func() {
		config.Stage = ""
		config.SecretsManagerClient = nil
		config.Secrets = nil
	})
	Describe("Initialize", func() {
		It("should initialize variables", func() {
			config.Initialize("test")
			Expect(config.Stage).To(Equal("test"))
			Expect(config.SecretsManagerClient).NotTo(BeNil())
		})
	})
	Describe("IsDebug", func() {
		It("should be true when stage == dev", func() {
			config.Stage = "dev"
			Expect(config.IsDebug()).To(BeTrue())
		})
		It("should be false when stage != dev", func() {
			config.Stage = "production"
			Expect(config.IsDebug()).To(BeFalse())
		})
	})

	Describe("Fetch Secrets", func() {
		It("should error when no client is found", func() {
			_, err := config.FetchSecrets("", "")
			Expect(err).ShouldNot(BeNil())
		})
		It("should error on invalid JSON returned", func() {
			config.SecretsManagerClient = MockSM{
				ExpectedResult: &secretsmanager.GetSecretValueOutput{
					SecretString: aws.String("{"),
				},
			}
			_, err := config.FetchSecrets("", "")
			Expect(err).ShouldNot(BeNil())
		})
		It("should return a decoded string on success", func() {
			expectedOutput := map[string]interface{}{"test": "test"}
			expectedInput := "{\"test\": \"test\"}"
			config.SecretsManagerClient = MockSM{
				ExpectedError: nil,
				ExpectedResult: &secretsmanager.GetSecretValueOutput{
					SecretString: aws.String(expectedInput),
				},
			}
			result, err := config.FetchSecrets("test", "test")
			Expect(err).Should(BeNil())
			Expect(result).To(Equal(expectedOutput))
		})
	})

	Describe("Get Secret", func() {
		It("should be empty when no client is found", func() {
			val := config.GetSecretString("test")
			Expect(val).Should(BeEmpty())
		})
		It("should be empty on invalid JSON returned", func() {
			config.SecretsManagerClient = MockSM{
				ExpectedResult: &secretsmanager.GetSecretValueOutput{
					SecretString: aws.String("{"),
				},
			}
			val := config.GetSecretString("test")
			Expect(val).Should(BeEmpty())
		})
		It("should return a value on success", func() {
			expectedOutput := "test"
			expectedInput := "{\"testing\": \"test\"}"
			config.SecretsManagerClient = MockSM{
				ExpectedError: nil,
				ExpectedResult: &secretsmanager.GetSecretValueOutput{
					SecretString: aws.String(expectedInput),
				},
			}
			result := config.GetSecretString("testing")
			Expect(result).To(Equal(expectedOutput))
		})
	})
})
