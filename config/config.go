package config

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-sdk-go/service/secretsmanager/secretsmanageriface"
	"log"
	"os"
	"strings"
)

var Stage string
var SecretsManagerClient secretsmanageriface.SecretsManagerAPI
var Secrets map[string]interface{}

func Initialize(stage string) {
	Stage = stage
	sess, err := session.NewSession()
	if err != nil {
		panic(err)
	}
	SecretsManagerClient = secretsmanager.New(sess)
}

func FetchSecrets(key, versionID string) (map[string]interface{}, error) {
	if SecretsManagerClient == nil {
		return nil, fmt.Errorf("no SecretsManager Client")
	}
	var input *secretsmanager.GetSecretValueInput
	if versionID != "" {
		input = &secretsmanager.GetSecretValueInput{
			SecretId:  aws.String(key),
			VersionId: aws.String(versionID),
		}
	} else {
		input = &secretsmanager.GetSecretValueInput{
			SecretId: aws.String(key),
		}
	}
	result, err := SecretsManagerClient.GetSecretValue(input)
	if err != nil {
		return nil, err
	}
	sec := map[string]interface{}{}
	err = json.Unmarshal([]byte(*result.SecretString), &sec)
	if err != nil {
		return nil, err
	}
	return sec, nil
}

type BotConfig struct {
	Timeout     int
	BotAdminIDs []int
}

func IsDebug() bool {
	return Stage == "dev"
}

func LogDebug(format string, v ...interface{}) {
	if IsDebug() {
		log.Printf(format, v...)
	}
}

func getSecret(key string) interface{} {
	if Secrets == nil {
		secretKey := fmt.Sprintf("%s/CrinkleNinjaBot/Secrets", Stage)
		versionID := os.Getenv(fmt.Sprintf("%s_SECRETS_VERSION_ID", strings.ToUpper(Stage)))
		var err error
		Secrets, err = FetchSecrets(secretKey, versionID)
		if err != nil {
			fmt.Printf("Failed to load Secrets: %v\n", err)
			Secrets = map[string]interface{}{}
		}
	}
	val, ok := Secrets[key]
	if !ok {
		fmt.Printf("Key '%s' not found in Secrets\n", key)
		return ""
	}
	return val
}

func GetSecretString(key string) string {
	val := getSecret(key)
	return val.(string)
}
