Contributing
============

This project uses Go, also known as golang, which is a cross-platform programming language developed by Google. Knowledge of Go is a requirement for contributing. Currently it is operating on Go v1.10.3 and uses `vgo` for dependency management. Please be aware that at the time of this writing `vgo` was still in an experimental stage. That means you may run into weird things!

Don't know much about Go? 
-------------------------
These websites should help you out.
* [A Tour of Go](https://tour.golang.org/)
* [Go By Example](https://gobyexample.com/)

Unsure what to use for development?
-----------------------------------
Here are some IDEs, editors, and articles:
* [Setup Go Development Environment with Visual Studio Code](https://rominirani.com/setup-go-development-environment-with-visual-studio-code-7ea5d643a51a)
* [GoLand by JetBrains](https://www.jetbrains.com/go/)
* Or any text editor!

To get started:
---------------
1. Head to https://golang.org/dl/ to download Go for your system. You should know your OS and arch.
2. After installing, you may need to [Set your GOPATH](https://github.com/golang/go/wiki/SettingGOPATH). You may also want to add your `GOPATH/bin` to your `PATH`.
3. You may need to set `GO111MODULE=on` in your environment.
4. If you don't have `git` installed, head to https://git-scm.com/downloads and grab it.
5. You may either `go get -u gitlab.com/fuzzy.little.devil/crinkleNinja` or manually create the path and check out the source.
6. Using `go get` automatically builds, otherwise `cd` into the new path and `go build`

If you followed the steps, you should see `go` download our two current dependencies and build the project successfully.

You should also run `go get -u golang.org/x/lint/golint` to install the Go Linter.

Before Committing
-----------------
* Run `go fmt ./...`
* Run `go vet ./...`
* Run `golint ./...`
* Fix any issues that show up!

Notes
-----
We use Go 1.11 experimental module support (formerly `vgo`)

[Modules - How to install and activate module support](https://github.com/golang/go/wiki/Modules#how-to-install-and-activate-module-support)



 
 