package google

import (
	"context"
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
	"log"
)

func FirstYouTubeResultFor(needle string) string {
	apiKey := config.GetSecretString("GoogleAPIKey")
	if apiKey == "" {
		log.Println("[YOUTUBE] API Key empty, unable to continue")
		return ""
	}
	service, err := youtube.NewService(context.Background(), option.WithAPIKey(apiKey))
	if err != nil {
		log.Printf("[YOUTUBE] Error creating new YouTube client: %v", err)
		return ""
	}

	response, err := service.Search.List([]string{"snippet"}).Q(needle).MaxResults(25).SafeSearch("moderate").Do()
	if err != nil {
		log.Printf("[YOUTUBE] Error making search API call: %v", err)
		return ""
	}

	for _, item := range response.Items {
		switch item.Id.Kind {
		case "youtube#video":
			return fmt.Sprintf("%s https://www.youtube.com/watch?v=%s", item.Snippet.Title, item.Id.VideoId)
		case "youtube#channel":
		case "youtube#playlist":
			continue
		}
	}
	log.Printf("[YOUTUBE] No results found for: %s", needle)
	return ""
}
