package tracks

import (
	"fmt"
	"log"
	"strings"
)

type TrackInfo struct {
	URL     string
	Artists []string
	Title   string
}

func (td *TrackInfo) String() string {
	return fmt.Sprintf("%s by %s", td.Title, strings.Join(td.Artists, ", "))
}

type TrackProvider interface {
	Matches(string) bool
	FindTrack(string) (*TrackInfo, error)
}

var providers = []TrackProvider{
	&SpotifyTrackProvider{},
	&TidalTrackProvider{},
}

func GetTrackFromURL(url string) (*TrackInfo, error) {
	for _, provider := range providers {
		if provider.Matches(url) {
			return provider.FindTrack(url)
		}
	}
	log.Printf("no provider found for %s", url)
	return nil, nil
}
