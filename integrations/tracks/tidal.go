package tracks

import (
	"context"
	"fmt"
	"github.com/tomjowitt/gotidal"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"regexp"
)

type TidalTrackProvider struct{}

var trackIdPattern = regexp.MustCompile(`https://tidal\.com/browse/track/([A-Za-z0-9]+).*`)

func (t *TidalTrackProvider) Matches(url string) bool {
	return trackIdPattern.MatchString(url)
}

func (t *TidalTrackProvider) FindTrack(url string) (*TrackInfo, error) {
	ctx := context.Background()
	matches := trackIdPattern.FindStringSubmatch(url)
	if len(matches) == 0 {
		return nil, fmt.Errorf("[Tidal] Unable to find track ID in %s\n", url)
	}
	clientID := config.GetSecretString("TidalClientID")
	clientSecret := config.GetSecretString("TidalClientSecret")
	if clientID == "" || clientSecret == "" {
		return nil, fmt.Errorf("[Tidal] ClientID and Secret not found, unable to continue")
	}
	client, err := gotidal.NewClient(clientID, clientSecret, "US")
	if err != nil {
		return nil, fmt.Errorf("[Tidal] Error creating client")
	}
	track, err := client.GetSingleTrack(ctx, matches[1])
	if err != nil {
		return nil, fmt.Errorf("[Tidal] Error getting track %s: %v\n", matches[1], err)
	}
	var artists []string
	for _, artist := range track.Artists {
		artists = append(artists, artist.Name)
	}
	return &TrackInfo{
		Artists: artists,
		Title:   track.Title,
		URL:     url,
	}, nil
}
