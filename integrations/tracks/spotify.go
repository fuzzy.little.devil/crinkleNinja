package tracks

import (
	"context"
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"regexp"

	"github.com/zmb3/spotify"
	"golang.org/x/oauth2/clientcredentials"
)

type SpotifyTrackProvider struct{}

var spotifyTrackIdPattern = regexp.MustCompile(`https://open\.spotify\.com/track/([A-Za-z0-9]+).*`)

func (s *SpotifyTrackProvider) Matches(url string) bool {
	return spotifyTrackIdPattern.MatchString(url)
}

func (s *SpotifyTrackProvider) FindTrack(url string) (*TrackInfo, error) {
	matches := spotifyTrackIdPattern.FindStringSubmatch(url)
	if len(matches) == 0 {
		return nil, fmt.Errorf("unable to find track id in %s\n", url)
	}
	clientId := config.GetSecretString("SpotifyClientID")
	clientSecret := config.GetSecretString("SpotifyClientSecret")
	if clientId == "" || clientSecret == "" {
		return nil, fmt.Errorf("[Spotify] ClientID and Secret not found, unable to continue")
	}
	cfg := &clientcredentials.Config{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		TokenURL:     spotify.TokenURL,
	}
	token, err := cfg.Token(context.Background())
	if err != nil {
		return nil, fmt.Errorf("[Spotify] couldn't get token: %v\n", err)
	}

	client := spotify.Authenticator{}.NewClient(token)
	ft, err := client.GetTrack(spotify.ID(matches[1]))
	if err != nil {
		return nil, fmt.Errorf("[Spotify] error getting track %s: %v\n", matches[1], err)
	}
	artists := make([]string, len(ft.Artists))
	for i, a := range ft.Artists {
		artists[i] = a.Name
	}
	return &TrackInfo{
		Artists: artists,
		Title:   ft.Name,
		URL:     url,
	}, nil
}
