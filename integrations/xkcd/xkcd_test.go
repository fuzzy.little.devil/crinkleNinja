package xkcd_test

import (
	"gitlab.com/fuzzy.little.devil/crinkleNinja/integrations/xkcd"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var validJsonNoExtra = []byte(`{
  "month": "1",
  "num": 1,
  "link": "",
  "year": "2006",
  "news": "",
  "safe_title": "Barrel - Part 1",
  "transcript": "[[A boy sits in a barrel which is floating in an ocean.]]\nBoy: I wonder where I'll float next?\n[[The barrel drifts into the distance. Nothing else can be seen.]]\n{{Alt: Don't we all.}}",
  "alt": "Don't we all.",
  "img": "https://imgs.xkcd.com/comics/barrel_cropped_(1).jpg",
  "title": "Barrel - Part 1",
  "day": "1"
}`)

var validJsonExtra = []byte(`{
  "month": "9",
  "num": 1110,
  "link": "",
  "year": "2012",
  "news": "",
  "safe_title": "Click and Drag",
  "transcript": "[[A character is dangling from a balloon.  All text appears in rectangular bubbles.]]\nCharacter: From the stories\nCharacter: I expected the world to be sad\nCharacter: And it was\n\nCharacter: And I expected it to be wonderful.\n\nCharacter: It was.\n\n((The last panel, unusually, is infinitely large, and this transcript is not wide enough to contain it.  The part you can see in a normal browser window goes as follows.))\n[[ The same character is dangling above the ground, next to an intricately drawn tree with no leaves. ]]\nCharacter: I just didn't expect it to be so \nbig\n.\n\n{{Title text: Click and drag.}}",
  "alt": "Click and drag.",
  "img": "https://imgs.xkcd.com/comics/click_and_drag.png",
  "title": "Click and Drag",
  "extra_parts": {
    "pre": "",
    "headerextra": "",
    "post": "\n<div class=\"map\"><div class=\"ground\"></div></div>\n<script type=\"text/javascript\" src=\"/s/a3c5de.js\"></script>\n<script type=\"text/javascript\" src=\"/s/d28668.js\"></script>\n",
    "imgAttr": ""
  },
  "day": "19"
}`)

var _ = Describe("XKCD Comic", func() {
	Describe("When NewStripFromJSON is given valid JSON", func() {
		s, err := xkcd.NewStripFromJSON(validJsonNoExtra)
		s2, err2 := xkcd.NewStripFromJSON(validJsonExtra)
		It("should not return an error", func() {
			Expect(err).Should(BeNil())
			Expect(err2).Should(BeNil())
		})
		It("should return a Strip", func() {
			Expect(s).ShouldNot(BeNil())
			Expect(s2).ShouldNot(BeNil())
		})
		It("should return a proper date", func() {
			Expect(s2.Date().Month()).To(Equal(time.September))
		})
	})
	Describe("When GetStripNumber is given a valid strip ID", func() {
		stripNum := 1
		s, err := xkcd.GetStripNumber(stripNum)
		It("should not error", func() {
			Expect(err).Should(BeNil())
		})
		It("should return a Strip", func() {
			Expect(s).ShouldNot(BeNil())
		})
		It("should have the correct strip number", func() {
			Expect(s.Num).To(Equal(stripNum))
		})
	})
	Describe("When GetRandomStrip is called", func() {
		s, err := xkcd.GetRandomStrip()
		It("should not error", func() {
			Expect(err).Should(BeNil())
		})
		It("should return a Strip", func() {
			Expect(s).ShouldNot(BeNil())
		})
	})
	Describe("When GetLatestStrip is called", func() {
		s, err := xkcd.GetLatestStrip()
		It("should not error", func() {
			Expect(err).Should(BeNil())
		})
		It("should return a Strip", func() {
			Expect(s).ShouldNot(BeNil())
		})
	})
})
