package xkcd_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestXKCD(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "XKCD Suite")
}
