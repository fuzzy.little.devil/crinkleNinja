package xkcd

import (
	"encoding/json"
	"fmt"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const JsonUrlTemplate string = "https://xkcd.com/%d/info.0.json"
const JsonLatestUrl string = "https://xkcd.com/info.0.json"
const RandomUrl string = "https://c.xkcd.com/random/comic/"

type Strip struct {
	Month      string      `json:"month"`
	Num        int         `json:"num"`
	Link       string      `json:"link"`
	Year       string      `json:"year"`
	News       string      `json:"news"`
	SafeTitle  string      `json:"safe_title"`
	Transcript string      `json:"transcript"`
	Alt        string      `json:"alt"`
	Img        string      `json:"img"`
	Title      string      `json:"title"`
	Day        string      `json:"day"`
	ExtraParts *ExtraParts `json:"extra_parts,omitempty"`
}

type ExtraParts struct {
	Pre         string `json:"pre"`
	HeaderExtra string `json:"headerextra"`
	Post        string `json:"post"`
	ImgAttr     string `json:"imgAttr"`
}

type ImageData struct {
	ContentType string
	Bytes       []byte
}

func (s Strip) Date() time.Time {
	if t, err := time.Parse("2006-1-2", fmt.Sprintf("%s-%s-%s", s.Year, s.Month, s.Day)); err != nil {
		return time.Time{}
	} else {
		return t
	}
}

func (s Strip) ImageData() (*ImageData, error) {
	resp, err := http.Get(s.Img)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &ImageData{
		ContentType: resp.Header.Get("Content-Type"),
		Bytes:       b,
	}, nil
}

func NewStripFromJSON(data []byte) (*Strip, error) {
	if config.IsDebug() {
		log.Printf("XCKD Debug: %s", string(data))
	}
	s := new(Strip)
	err := json.Unmarshal(data, s)
	return s, err
}

func GetRandomStrip() (*Strip, error) {
	resp, err := http.Get(RandomUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	url := resp.Request.URL
	num, err := strconv.Atoi(strings.Trim(url.Path, "/"))
	if err != nil {
		return nil, err
	}
	return GetStripNumber(num)
}

func GetStrip(url string) (*Strip, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return NewStripFromJSON(data)
}

func GetStripNumber(num int) (*Strip, error) {
	return GetStrip(fmt.Sprintf(JsonUrlTemplate, num))
}

func GetLatestStrip() (*Strip, error) {
	return GetStrip(JsonLatestUrl)
}
