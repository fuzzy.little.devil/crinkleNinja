// main
package main

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/config"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/database"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/entities"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/commands"
	evt "gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/events"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/keyboards"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/messaging"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/model"
	"gitlab.com/fuzzy.little.devil/crinkleNinja/telegram/triggers"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type Service string

const (
	TelegramService Service = "telegram"
)

func main() {
	lambda.Start(HandlerFunc)
}

func HandlerFunc(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	config.Initialize(request.StageVariables["lambdaAlias"])
	servicePath := strings.Split(strings.Trim(request.Path, "/"), "/")
	database.InitializeDatabase()
	defer entities.Flush()
	if len(servicePath) > 0 {
		switch Service(servicePath[0]) {
		case TelegramService:
			commands.InitCommands()
			triggers.InitTriggers()
			keyboards.InitKeyboards()
			telegram.RegisterHandler("TriggerHandler", evt.TriggerEvent.Invoke)
			telegram.RegisterHandler("KeyboardHandler", evt.ReplyKeyboardEvent.Invoke)
			telegram.RegisterHandler("CommandHandler", evt.CommandEvent.Invoke)
			return HandleTelegramRequest(request)
		}
	}
	return events.APIGatewayProxyResponse{StatusCode: http.StatusNotFound}, fmt.Errorf("resource not found")
}

func handleError(err error) (events.APIGatewayProxyResponse, error) {
	log.Printf("ERROR: %v", err)
	return events.APIGatewayProxyResponse{StatusCode: http.StatusInternalServerError}, err
}

func HandleTelegramRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	idsStr := strings.Split(os.Getenv("BOT_ADMINS"), ",")
	var adminIds []int
	for _, idStr := range idsStr {
		adminId, err := strconv.Atoi(strings.TrimSpace(idStr))
		if err == nil {
			adminIds = append(adminIds, adminId)
		} else {
			log.Printf("invalid AdminID in BOT_ADMINS: %s", idStr)
		}
	}

	b, err := telegram.NewBot(&config.BotConfig{
		BotAdminIDs: adminIds,
		Timeout:     60,
	})
	if err != nil {
		return handleError(err)
	}
	var update tgbotapi.Update
	err = json.Unmarshal([]byte(request.Body), &update)
	if err != nil {
		return handleError(err)
	}
	if update.Message == nil {
		// ignore bad messages
		return events.APIGatewayProxyResponse{StatusCode: http.StatusOK}, nil
	}
	msg := messaging.NewMessageFromUpdate(update)
	model.AddChatFromMessage(msg)
	model.AddUserFromMessage(msg)
	model.CurrentChat.UpdateChatAdmins(b)
	if model.CurrentUser != nil && model.CurrentChat != nil {
		model.CurrentUser.CurrentChatAdmin = model.CurrentChat.IsAdmin(model.CurrentUser.ID) || model.CurrentChat.IsPrivate()
	}
	if config.IsDebug() {
		fmt.Printf("Received:\n%s", request.Body)
	}
	b.ProcessMessage(msg)
	return events.APIGatewayProxyResponse{StatusCode: http.StatusOK}, nil
}
